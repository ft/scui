;; scui - scheme curses user interface

;; Jobs:
;;
;;  - Layouts: Take terminal and divide it into separate compartments in which
;;    to put windows. Layouts have to be changable at runtime and terminal
;;    resizing has to be properly supported.
;;
;;  - Input: Provide infrastructure for user-definable actions to arbitrarily
;;    long key sequences, with different keymaps for different windows a
;;    default fallback keymap, special-purpose keymaps and additional
;;    user-definable keymaps.
;;
;;  - Line (possibly multi-line) editor: Extensible command-line-style editor
;;    with tab-completion support.
;;
;;  - Widgets: Highlevel applications to fill windows from layouts with. For
;;    example: line-wise window with selection bar and search support; file
;;    browser or key-value editor on top of that. Vertical sliders; horizontal
;;    sliders... etc.
;;
;;  - Formatted output: Like `format' but with support for colours, text
;;    alignment (left, right at least).

;;; --------------------
;;; --- Example Code ---
;;; --------------------

(define pager-layout
  (window #:target colourful-pager))

(define less-like-pager-layout
  (vertical (window #:target colourful-pager)
            (window #:target pager-status)))

(define cmc/tree-like-layout
  (vertical (window #:target cmc-title)
            (horizontal (window #:size (/ 1 3)
                                #:target cmc-tree)
                        (separator)
                        (window #:size 'fill
                                #:target cmc-album-list))
            (window #:target cmc-current-track)
            (window #:target cmc-status)
            (window #:target cmc-status-aux)
            (window #:target cmc-commandline/echoarea)))

;;; -------------------
;;; --- Actual Code ---
;;; -------------------

(define-record-type ui/layout
  (make-ui/layout terminal contents)
  ui/layout?
  (terminal uil/get-terminal)
  (contents uil/get-contents uil/set-contents!))
