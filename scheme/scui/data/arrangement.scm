;; Copyright (c) 2019 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in LICENCE.

(define-module (scui data arrangement)
  #:use-module (ice-9 control)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:export (make-dimension-hint
            make-window
            dim:type
            dim:arg
            fold-hint/same
            fold-hint/sum))

;; Windows (the payload in arrangement elements) define minimal sizes.
(define-immutable-record-type <size-minimum>
  (make-size-minimum* width height)
  size-minimum?
  (width size:min:width)
  (height size:min:height))

(define* (make-size-minimum #:key (width 1) (height 1))
  (make-size-minimum* width height))

(define-immutable-record-type <dimension-hint>
  (make-dimension-hint* type value argument)
  dimension-hint?
  (type dim:type)
  (value dim:value dim:new-value)
  (argument dim:arg dim:new-arg))

(define (generate-dimension-hint dim)
  (match dim
    (('ratio value #:minimum min) (make-dimension-hint* 'ratio value min))
    ((type value) (make-dimension-hint* type value 1))
    (else (make-dimension-hint* 'minimum 1 1))))

(define (make-dimension-hint . lst)
  (generate-dimension-hint lst))

(define-immutable-record-type <size-hint>
  (make-size-hint* width height)
  size-hint?
  (width size:width)
  (height size:height))

(define* (make-size-hint #:key (width '()) (height '()))
  (make-size-hint* (generate-dimension-hint width)
                   (generate-dimension-hint height)))

(define-immutable-record-type <arrangement-controller>
  (make-arrangement-controller w h)
  arrangement-controller?
  (w actrl:width)
  (h actrl:height))

(define-immutable-record-type <geometry-controller>
  (make-geometry-controller geometry controller)
  geometry-controller?
  (geometry gctrl:geometry gctrl:new-geometry)
  (controller gctrl:controller gctrl:new-controller))

(define-immutable-record-type <window>
  (make-window* min-size)
  window?
  ;; <size-minimum>
  (min-size win:min-size))

(define* (make-window #:key size)
  (make-window* (or size (make-size-hint))))

(define-immutable-record-type <element>
  (make-element geometry size-hint window)
  element?
  (geometry el:geometry el:new-geometry)
  (size-hint el:size-hint)
  (window el:window))

(define (invalid-hint v lst)
  (make-dimension-hint* 'invalid v lst))

(define (unknown-hint this prev)
  (invalid-hint (dim:value prev)
                `((reason . unknown-dimension-hint)
                  (prev . ,prev)
                  (current . ,this))))

(define (same:with-prev:exact return this prev)
  (case (dim:type this)
    ((exact)
     (if (= (dim:value this) (dim:value prev))
         prev
         (return (invalid-hint (dim:value prev)
                               `((reason . exact-hint-mismatch)
                                 (prev . ,prev)
                                 (current . ,this))))))
    ((minimum)
     (if (<= (dim:value this) (dim:value prev))
         prev
         (return (invalid-hint (dim:value prev)
                               `((reason . cannot-fit-minimum-into-exact)
                                 (prev . ,prev)
                                 (current . ,this))))))
    ((ratio) (same:with-prev:exact return
                              (make-dimension-hint 'minimum (dim:arg this))
                              prev))
    (else (return (unknown-hint this prev)))))

(define (same:with-prev:minimum return this prev)
  (case (dim:type this)
    ((minimum) (dim:new-value this (max (dim:value this) (dim:value prev))))
    ((exact)
     (if (>= (dim:value this) (dim:value prev))
         this
         (return (invalid-hint (dim:value prev)
                               `((reason . cannot-fit-exact-into-minimum)
                                 (prev . ,prev)
                                 (current . ,this))))))
    ((ratio) (make-dimension-hint 'minimum (max (dim:value prev)
                                                (dim:arg this))))
    (else (return (unknown-hint this prev)))))

(define (fold-hint/same hints)
  (call/ec
   (lambda (return)
     (fold (lambda (this prev)
             (case (dim:type prev)
               ((exact) (same:with-prev:exact return this prev))
               ((minimum) (same:with-prev:minimum return this prev))
               (else (return (unknown-hint this prev)))))
           (make-dimension-hint 'minimum 1)
           hints))))

(define (sum:with-prev:exact return this prev)
  (case (dim:type this)
    ((exact minimum) (dim:new-value this (+ (dim:value prev) (dim:value this))))
    ((ratio) (make-dimension-hint 'minimum (+ (dim:value prev) (dim:arg this))))
    (else (return (unknown-hint this prev)))))

(define (sum:with-prev:minimum return this prev)
  (case (dim:type this)
    ((minimum) (dim:new-value this (+ (dim:value this) (dim:value prev))))
    ((exact) (dim:new-value prev (+ (dim:value this) (dim:value prev))))
    ((ratio) (make-dimension-hint 'minimum (+ (dim:arg this) (dim:value prev))))
    (else (return (unknown-hint this prev)))))

(define (fold-hint/sum hints)
  (call/ec
   (lambda (return)
     (fold (lambda (this prev)
             (case (dim:type prev)
               ((exact) (sum:with-prev:exact return this prev))
               ((minimum) (sum:with-prev:minimum return this prev))
               (else (return (unknown-hint this prev)))))
           (make-dimension-hint 'exact 0)
           hints))))

(define vertical-split
  (make-arrangement-controller fold-hint/same fold-hint/sum))

(define horizontal-split
  (make-arrangement-controller fold-hint/sum fold-hint/same))
