;; Copyright (c) 2019 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

(define-module (scui data geometry)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 optargs)
  #:export (make-ui/coordinate
            ui/coordinate?
            coordinate:x
            coordinate:y
            make-ui/rectangle
            ui/rectangle?
            rectangle:height
            rectangle:width
            make-ui/geometry*
            make-ui/geometry
            ui/geometry?
            g:coordinate
            g:rectangle))

(define-immutable-record-type <ui/coordinate>
  (make-ui/coordinate x y)
  ui/coordinate?
  (x coordinate:x)
  (y coordinate:y))

(define-immutable-record-type <ui/rectangle>
  (make-ui/rectangle height width)
  ui/rectangle?
  (height rectangle:height)
  (width rectangle:width))

(define-immutable-record-type <ui/geometry>
  (make-ui/geometry* coordinate rectangle)
  ui/geometry?
  (coordinate g:coordinate)
  (rectangle g:rectangle))

(define* (make-ui/geometry #:key (x 0) (y 0) (height 1) (width 1))
  (make-ui/geometry* (make-ui/coordinate x y)
                     (make-ui/rectangle height width)))
