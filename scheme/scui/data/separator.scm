;; Copyright (c) 2014 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

(define-module (scui data separator)
  #:export (separator
            ui/separator?
            ui:sep/get-direction
            ui:sep/set-direction!))

(define-record-type <ui/separator>
  (make-ui/separator direction)
  ui/separator?
  (direction ui:sep/get-direction ui:sep/set-direction!))

(define (separator)
  (make-ui/separator #f))
