;; Copyright (c) 2016 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in LICENCE.

(define-module (scui data size-hint)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:export (height-hint
            hint->demand
            make-size-hint
            size-hint?
            width-hint))

(define (wish? data)
  (or (symbol? data)
      (and (number? data)
           (zero? (imag-part data))
           (or (exact-integer? data)
               (and (rational? data)
                    (< data 1)
                    (> data 0))
               (and (inexact? data)
                    (< data 100.0)
                    (> data 0.0))))))

(define* (make-size-hint* w h #:key (min-width 1) (min-height 1))
  (list (cons w min-width) (cons h min-height)))

(define* (make-size-hint w h #:key (min-width 1) (min-height 1))
  (unless (and (exact-integer? min-width)
               (exact-integer? min-height)
               (wish? w)
               (wish? h))
    (throw 'scui/broken-hint-arguments w h min-width min-height))
  (make-size-hint* w h #:min-width min-width #:min-height min-height))

(define (size-hint? x)
  (match x
    (((w . mw) (h . mh))
     (and (exact-integer? mw)
          (exact-integer? mh)
          (wish? w)
          (wish? h)))
    (else #f)))

(define (width-hint hint)
  (car hint))

(define (height-hint hint)
  (cadr hint))

(define (hint->demand data)
  (define (convert-hint wish minimum)
    (cons (if (exact-integer? wish) wish '*) minimum))
  (match data
    (((w . mw) (h . mh))
     (list (convert-hint w mw)
           (convert-hint h mh)))
    ((wish . minimum)
     (convert-hint wish minimum))
    (else (throw 'scui/not-a-hint data))))
