;; Copyright (c) 2014 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

(define-module (scui data split)
  #:export (make-ui/split
            ui/split?
            uis/width
            uis/height
            uis/type
            vertical
            horizontal))

(define-record-type <ui/split>
  (make-ui/split type width height options)
  ui/split?
  (width uis/get-width)
  (height uis/get-height)
  (type uis/get-type uis/set-type!))

(define (new-split type . items)
  (make-tree #:payload (make-ui/split type)
             #:children items))

(define (vertical . items)
  (apply new-split (cons 'vertical items)))

(define (horizontal . items)
  (apply new-split (cons 'horizontal items)))
