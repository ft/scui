;; Copyright (c) 2014 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

(define-module (scui data terminal)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-9)
  #:use-module (ncurses curses)
  #:use-module (scui user-key)
  #:export (make-ui/terminal
            open-ui/terminal!
            close-ui/terminal!
            suspend-ui!
            ui/terminal?
            uit/type
            uit/content
            uit/height
            uit/width
            uit/std-screen
            uit/set-width!
            uit/set-height!))

(define-record-type <ui/terminal>
  (make-ui/terminal* type non-blocking?
                     std-screen needs-update? state
                     width height
                     content)
  ui/terminal?
  (type uit/type)
  (non-blocking? uit/non-blocking?)
  (std-screen uit/std-screen uit/set-std-screen!)
  (needs-update? uit/needs-update? uit/set-needs-update!)
  (state uit/state uit/set-state!)
  (width uit/width uit/set-width!)
  (height uit/height uit/set-height!)
  (content uit/content uit/set-content!))

(define* (make-ui/terminal #:key
                           (type (getenv "TERM"))
                           (non-blocking? #f)
                           (content #f))
  (make-ui/terminal* type non-blocking? #f 'needs-update 'needs-init 0 0 #f))

(define (initialise-terminal! terminal)
  "Initialise the terminal to a state, that scui expects. It also takes care of
initially updating user-defined keys."
  (setlocale LC_ALL "")
  (let ((curses-standard-screen (initscr)))
    (keypad! curses-standard-screen #t)
    (scrollok! curses-standard-screen #t)
    (nonl!)
    (noecho!)
    (raw!)
    (update-user-keys)
    (when (uit/non-blocking? terminal)
      (nodelay! curses-standard-screen #t))
    (uit/set-std-screen! terminal curses-standard-screen)
    (uit/set-state! terminal 'initialised)
    terminal))

(define (uit/needs-update! terminal)
  (uit/set-needs-update! terminal #t))

(define* (open-ui/terminal! terminal #:key content)
  (initialise-terminal! terminal)
  (when content
    (uit/set-content! terminal content))
  (sigaction SIGWINCH (lambda args
                        (uit/set-width! terminal (cols))
                        (uit/set-height! terminal (lines))
                        (uit/needs-update! terminal)))
  terminal)

(define* (close-ui/terminal! terminal #:key name)
  "Bring the terminal back to the state the program was before curses was
initialised. Also print an exit message.

You would call this function after you exited your main-loop shortly before
exiting your program."
  (endwin)
  (sigaction SIGWINCH #f)
  (uit/set-state! terminal 'needs-init)
  (when name
    (format #t "~a exits.~%" name)))

(define* (suspend-ui! terminal #:key name)
  "Send SIGSTOP to ourself, to suspend the program. Scui's curses
initialisation disables terminal key-bindings that send signals to the running
program. That includes Ctrl-Z, which usually suspends a program. This function
can be used to savely suspend the program, by explicitly handling terminal
state before and after manually applying the required signal on the program's
own pid."
  (endwin)
  (uit/set-state! terminal 'suspended)
  (when name
    (format #t "~a suspended.~%~a~%"
            name
            "Bring it back using your shell's job control features."))
  (raise SIGSTOP)
  (refresh))
