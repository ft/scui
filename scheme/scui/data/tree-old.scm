;; Copyright (c) 2013 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

;; In `scui', the way screen estate is compartmentalised, is represented as a
;; tree. Any node can have zero or more children. As a tree, each node in this
;; data structure are exactly one parent node (except for the parent-node,
;; which has no parent at all). Internally, each node has three properties: A
;; list of children, a link to its parent node and the node's payload.

(define-module (scui data tree-old)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 control)
  #:use-module (ice-9 optargs)
  #:export (make-leaf-node
            make-tree
            tree?
            node?
            node-is-root?
            node-is-leaf?
            node-ref
            node-ref*
            node-children
            node-parent
            node-payload
            node-first-sibling
            node-last-sibling
            node-left-sibling
            node-right-sibling
            node-append!
            node-cons!
            node-insert!
            node-move!
            node-remove!
            node-rotate-left!
            node-rotate-right!
            node-swap!
            node-to-beginning!
            node-to-end!
            append-children!
            cons-children!
            tree-copy
            traverse-fold
            traverse-map
            traverse-filter
            traverse-search
            flatten))

(define (call-or-return proc arg)
  "If PROC is defined, assume it is a function of one argument and call it with
ARG as its argument and return the value that call produces. Otherwise, return
ARG as is."
  (if proc (proc arg) arg))

(define-record-type <node>
  (make-node parent children payload)
  node?
  (payload node-payload node-payload-set!)
  (parent node-parent node-parent-set!)
  (children node-children node-children-set!))

(define* (make-tree #:key
                    (children '())
                    (payload #f))
  "Make a new tree. If specified via the optional PAYLOAD keyword argument, the
new node's payload will be set accordingly (default: #f).

If the optional CHILDREN keyword argument is used, `make-tree' may be used in a
nested way to specify a complete tree of arbitrary complexity. The argument has
to be a list of nested `make-tree' calls.

Example:

    (make-tree
     #:payload 'foo
     #:children (list (make-tree
                       #:payload 'bar
                       #:children (list (make-tree #:payload 'baz)
                                        (make-tree #:payload 'ninja)
                                        (make-tree #:payload 'wilma)))
                      (make-tree #:payload 'lonely)))

Result:             foo
                   /   \\
                  /     \\
                bar     lonely
               / | \\
              /  |  \\
             /   |   \\
            /    |    \\
         baz   ninja   wilma"
  (let ((new (make-node #f children payload)))
    (map (lambda (c)
           (node-parent-set! c new))
         children)
    new))

(define (make-leaf-node payload)
  (make-node #f '() payload))

(define (tree? obj)
  "Like `node?' but also requires OBJ to be a root-node, thus addressing the
whole tree."
  (and (node? obj)
       (node-is-root? obj)))

(define (node-is-root? node)
  "Returns #t if NODE is the root of a tree; #f otherwise."
  (eq? #f (node-parent node)))

(define (node-is-leaf? node)
  "Returns #t if NODE is a leaf of a tree; #f otherwise."
  (null? (node-children node)))

(define (add-children node lst-payload proc)
  "Add children to NODE with each payload set to one of the entries in
LST-PAYLOAD. PROC is used to assemble the new list of children.

This is a utility function used by `append-children!' and `cons-children!'."
  (let ((old (node-children node))
        (new (map (lambda (x)
                    (make-node node '() x))
                  lst-payload)))
    (node-children-set! node (proc new old))))

(define (append-children! node lst-payload)
  "Add child-nodes for payloads in `lst-payload' to the end of the existing
children of the parent-node `node'."
  (add-children node lst-payload (lambda (n o)
                                   (append o n))))

(define (cons-children! node lst-payload)
  "Add child-nodes for payloads in `lst-payload' to the beginning of the
existing children of the parent-node `node'."
  (add-children node lst-payload (lambda (n o)
                                   (append n o))))

(define (node-is-in-ancestry? maybe-ancestor node)
  "Check if MAYBE-ANCESTOR is an ancestor of NODE.
If so, return #t; #f otherwise."
  (if (memq maybe-ancestor (node-ancestry node))
      #t
      #f))

(define (node-ancestry node)
  "Return a list of parent nodes, starting at NODE's parent node up to the
tree's root node. If NODE is a root-node, the empty list is returned."
  (let next ((cur node) (acc '()))
    (if (node-is-root? cur)
        acc
        (let ((parent (node-parent cur)))
          (next parent (cons parent acc))))))

(define (node-find-in-children node)
  "Find NODE in the list of children of NODE's parent node and return a
sub-list that is headed by NODE. Return #t if NODE is a root-node (and
therefore has no parent node)."
  (if (node-is-root? node)
      #t
      (let next ((nodes (node-children (node-parent node))))
        (if (eq? node (car nodes))
            nodes
            (next (cdr nodes))))))

(define* (node-left-sibling node #:key (post-proc car))
  "Return the left sibling of NODE. If NODE is a root-leaf, #t is returned. If
NODE is the left-most sibling, #f is returned.

If the optional POST-PROC parameter is set to a function of one argument, it is
used on a sub-list of child-nodes, of which the head is the left-sibling of
NODE. The default value of this parameter is `car', which results in the
behaviour described above.

If POST-PROC is set to #f, the list that is headed by the sibling in question
is returned as is."
  (if (node-is-root? node)
      #t
      (let next ((nodes (node-children (node-parent node)))
                 (prev #f))
        (if (eq? node (car nodes))
            (if prev (call-or-return post-proc prev) prev)
            (next (cdr nodes) nodes)))))

(define* (node-right-sibling node #:key (post-proc car))
  "Return the right sibling of NODE. If NODE is a root-leaf, #t is returned. If
NODE is the right-most sibling, #f is returned.

If the optional POST-PROC parameter is set to a function of one argument, it is
used on a sub-list of child-nodes, of which the head is the right-sibling of
NODE. The default value of this parameter is `car', which results in the
behaviour described above.

If POST-PROC is set to #f, the list that is headed by the sibling in question
is returned as is."
  (let ((node-list (node-find-in-children node)))
    (if (not (list? node-list))
        node-list
        (if (= (length node-list) 1)
            #f
            (call-or-return post-proc (cdr node-list))))))

(define* (node-first-sibling node #:key (post-proc car))
  "Return the first entry the list of siblings of NODE. Return #t if NODE is a
root-node. Return #f if NODE *is* the first entry in the list of siblings.

If the optional POST-PROC parameter is set to a function of one argument, it is
used on the list of child-nodes, of which the head is indeed the first sibling
of NODE. The default value of this parameter is `car', which results in the
behaviour described above.

If POST-PROC is set to #f, the list that is headed by the sibling in question
is returned as is."
  (if (node-is-root? node)
      #t
      (let ((nodes (node-children (node-parent node))))
        (if (eq? node (car nodes))
            #f
            (call-or-return post-proc nodes)))))

(define* (node-last-sibling node #:key (post-proc car))
  "Return the last entry the list of siblings of NODE. Return #t if NODE is a
root-node. Return #f if NODE *is* the last entry in the list of siblings.

If the optional POST-PROC parameter is set to a function of one argument, it is
used on the list of child-nodes, of which the head is the last sibling of NODE.
The default value of this parameter is `car', which results in the behaviour
described above.

If POST-PROC is set to #f, the list that is headed by the sibling in question
is returned as is."
  (if (node-is-root? node)
      #t
      (let ((last-list (last-pair (node-children (node-parent node)))))
        (if (eq? node (car last-list))
            #f
            (call-or-return post-proc last-list)))))

(define (node-ref node idx)
  "Get the child node described by `idx' (starting at 0) of NODE. If idx
is out of range, #f is returned."
  (let ((nodes (node-children node)))
    (if (< idx (length nodes))
        (list-ref nodes idx)
        #f)))

(define (node-ref* node idx)
  "Like `node-ref', but instead of returning the whole node, only return
the payload of the indexed node."
  (let ((target (node-ref node idx)))
    (if target
        (node-payload target)
        #f)))

(define (list-insert! lst idx item)
  (if (= idx 0)
      (let ((old (car lst))
            (rest (cdr lst)))
        (set-car! lst item)
        (set-cdr! lst (cons old rest)))
      (let* ((this (list-tail lst (- idx 1)))
             (rest (cdr this)))
        (set-cdr! this (cons item rest)))))

(define (insertion-index dest-tree idx)
  (let* ((len (length (node-children dest-tree)))
         (idx (if (>= idx 0)
                  idx
                  (+ 1 len idx))))
    (if (or (> 0 idx)
            (> idx len))
        #f
        idx)))

(define (unsafe-node-insert! node dest-tree idx)
  (let ((children (node-children dest-tree)))
    (if (null? children)
        (node-cons! node dest-tree)
        (begin (list-insert! children idx node)
               (node-parent-set! node dest-tree))))
  #t)

(define (node-insert! node dest-tree idx)
  "Insert NODE into the list of child-nodes of DEST-TREE at index IDX.
Returns #t if the node was inserted; #f otherwise. If IDX is negative,
insertion is indexed from the end of the list, -1 being the end of the
list."
  (let* ((idx (insertion-index dest-tree idx)))
    (if (not idx)
        #f
        (unsafe-node-insert! node dest-tree idx))))

(define (node-move! node dest-tree idx)
  "Move NODE from its current parent-node to DEST-TREE at index IDX of
DEST-TREE's list of child-nodes. IDX behaves as the same parameter of the
`node-insert' function. If NODE is the root node or if NODE is a non-leaf-node
that is in DEST-TREE's ancestry, the node cannot be moved. If NODE was actually
moved, #t is returned; #f otherwise."
  (cond ((node-is-root? node) #f)
        ((or (node-is-leaf? node)
             (not (node-is-in-ancestry? node dest-tree)))
         (let ((idx (insertion-index dest-tree idx)))
           (if (not idx)
               #f
               (begin (node-remove! node)
                      (unsafe-node-insert! node dest-tree idx)
                      #t))))
        (#t #f)))

(define (node-append! node dest-tree)
  "Adds NODE to the end of DEST-TREE's child-nodes."
  (node-parent-set! node dest-tree)
  (node-children-set! dest-tree
                      (append (node-children dest-tree)
                              (list node))))

(define (node-cons! node dest-tree)
  "Adds NODE to the beginning of DEST-TREE's child-nodes."
  (node-parent-set! node dest-tree)
  (node-children-set! dest-tree (cons node (node-children dest-tree))))

(define (node-remove! node)
  "Remove NODE from its parent's list of children and return node as a
root-node; ie. with no parent set. If called with a root-node, that node is
returned unchanged."
  (if (node-is-root? node)
      node
      (begin
        (let ((parent (node-parent node)))
          (node-children-set! parent
                              (delq node (node-children parent))))
        (node-parent-set! node #f)
        node)))

(define (perform-swap list-a list-b)
  "LIST-A and LIST-B are sub-lists of nodes, with LIST-A's head being node-a
and LIST-B's head being node-b. This function then swaps node-a and node-b.
The return-value of this function is undefined."
  (let ((a (car list-a))
        (b (car list-b)))
    (set-car! list-a b)
    (set-car! list-b a)))

(define* (node-swap! node-a node-b #:key (with-subtrees #t))
  "Let NODE-A and NODE-B swap places. The function returns #t if it actually
swapped NODE-A and NODE-B; if a situation prevents this from happening, it
returns #f.

Swapping out nodes of a tree structure leads to a number of different cases:

1. When NODE-A and NODE-B point to the same node, no action has to be taken.
   This is the trivial case.

2. If sub-trees are *NOT* swapped out along with their parent nodes (which
   is the case if the option keyword parameter WITH-SUBTREES is set to #f),
   swapping the nodes *ALWAYS* works: The nodes exchange their places, while
   their children stay where they were, now becoming the children of the
   respective other node.

All other cases cover situations where the function is supposed to swap nodes
*WITH* their sub-trees (ie. when WITH-SUBTREES is set to #t, which is the
default behaviour).

3. If NODE-A and NODE-B both have the same parent, swapping them out works with
   no further actions required. This case does not fail.

4. If NODE-A and NODE-B are both leaf-nodes, but with different parent nodes,
   after swapping the actual nodes, the respective parent-node links must be
   swapped as well.

5. If one of the nodes is a leaf-node but the other isn't, the non-leaf node
   may not be among the ancestors of the leaf-node. If this requirement is met,
   the required actions are the same as with case 4. Otherwise no action is
   taken.

6. If both nodes are non-leaf nodes, neither node may be among the ancestors of
   the other one. If these requirements are met, the procedure is the same as
   with case 5."
  ;; Implementation note: In the cases described above, in which sub-trees do
  ;; not have to be moved around, it would be easier to only swap the payload
  ;; from one node to the other and visa versa. This implementation does not
  ;; take this shortcut, because if it did, a node that was involved in such a
  ;; swap would look unchanged to `eq?' even though it was supposed to be
  ;; swapped elsewhere. In actually swapping the node objects, `eq?' remains a
  ;; stable way to identify nodes with.

  (define (find-and-swap a b)
    ;; Find A and B in their parent's child-list and actually swap them out.
    (perform-swap (node-find-in-children a) (node-find-in-children b)))

  (define (swap-with-root root other)
    (set-car! (node-find-in-children other) root))

  (define (family-swap grand-parent parent child)
    ;; When swapping a child-node with its direct parent, special care needs to
    ;; be taken, because if we'd link the parent's parent-link to the child's old
    ;; parent-link, we'd link the old parent to itself.
    (if (node-is-root? parent)
        (swap-with-root parent child)
        (find-and-swap parent child))
    (let ((children-child (node-children child)))
      (node-children-set! child (node-children parent))
      (node-children-set! parent children-child)
      (node-parent-set! parent child)
      (node-parent-set! child grand-parent)))

  (define (payload-swap a b)
    ;; Swap nodes A and B by putting A's children and parent into B and visa
    ;; versa. This kind of swap always works with two non-identical nodes.
    ;;
    ;; Special care has to be taken if one node is the parent of the other.
    ;; Also, if one of the nodes is the root-node, `swap-with-root' needs to be
    ;; used instead of `find-and-swap', because the function would call
    ;; `node-find-in-children' with the root-node, which would break.
    (let ((parent-a (node-parent a))
          (parent-b (node-parent b)))
      (cond ((eq? a parent-b)
             (family-swap parent-a a b))
            ((eq? b parent-a)
             (family-swap parent-b b a))
            (#t
             (let ((children-a (node-children a)))
               (cond ((node-is-root? a) (swap-with-root a b))
                     ((node-is-root? b) (swap-with-root b a))
                     (#t (find-and-swap a b)))
               (node-children-set! a (node-children b))
               (node-children-set! b children-a)
               (node-parent-set! a (node-parent b))
               (node-parent-set! b parent-a)))))
    #t)

  (define (reparent-swap a parent-a b parent-b)
    ;; Here we need to swap the parent links, because they differ. The
    ;; child-lists remain as they are, because this is a sub-tree swap.
    (find-and-swap a b)
    (node-parent-set! a parent-b)
    (node-parent-set! b parent-a))

  (define (swap-with-subtrees a parent-a b parent-b)
    ;; When sub-trees are to be swapped along with their parent-node, the cases
    ;; in the function definition need to be taken care of.
    (let ((ret #t))
      (cond ((eq? parent-a parent-b)
             (find-and-swap a b))
            ((and (node-is-leaf? a)
                  (node-is-leaf? b))
             (reparent-swap a parent-a b parent-b))
            ((and (node-is-leaf? a)
                  (not (node-is-leaf? b)))
             (if (node-is-in-ancestry? b a)
                 (set! ret #f)
                 (reparent-swap a parent-a b parent-b)))
            ((and (not (node-is-leaf? a))
                  (node-is-leaf? b))
             (if (node-is-in-ancestry? a b)
                 (set! ret #f)
                 (reparent-swap a parent-a b parent-b)))
            (#t
             (if (or (node-is-in-ancestry? a b)
                     (node-is-in-ancestry? b a))
                 (set! ret #f)
                 (reparent-swap a parent-a b parent-b))))
      ret))

  (if (eq? node-a node-b)
      'same-nodes
      (if with-subtrees
          (swap-with-subtrees node-a (node-parent node-a)
                              node-b (node-parent node-b))
          (payload-swap node-a node-b))))

(define (node-rotate! node finder flipper)
  (let ((parent (node-parent node)))
    (if (or (node-is-root? node)
            (= 1 (length (node-children parent))))
        #f
        (let ((right (finder node #:post-proc #f)))
          (if (not right)
              (begin (node-remove! node)
                     (flipper node parent))
              (perform-swap right (node-find-in-children node)))
          #t))))

(define (node-rotate-left! node)
  "Take NODE and make it swap places with its left sibling. If NODE is the
left-most node among the siblings, remove it from its place and move it to the
end of the list. Returns #t if NODE was moved; #f otherwise."
  (node-rotate! node node-left-sibling node-append!))

(define (node-rotate-right! node)
  "Take NODE and make it swap places with its right sibling. If NODE is the
right-most node among the siblings, remove it from its place and move it to the
front of the list. Returns #t if NODE was moved; #f otherwise."
  (node-rotate! node node-right-sibling node-cons!))

(define (node-to! node where)
  (if (node-is-root? node)
      #f
      (let ((all (where node #:post-proc #f)))
        (if (not all)
            #f
            (begin (perform-swap all (node-find-in-children node))
                   #t)))))

(define (node-to-beginning! node)
  "Move NODE to the beginning of its parent's children. Returns #t if the node
was moved; #f otherwise."
  (node-to! node node-first-sibling))

(define (node-to-end! node)
  "Move NODE to end of its parent's children. Returns #t if the node was moved;
#f otherwise."
  (node-to! node node-last-sibling))

(define* (tree-copy tree #:key (payload (lambda (x) x)))
  "Take a copy of the node pointed at by TREE with all its children.

The optional PAYLOAD keyword-parameter maybe used to specify a function of one
argument, that creates a copy of its argument and returns that copy. This may
be used to - for example when the payload of the tree are strings - create
actual copies of payload strings in copied trees.

The default PAYLOAD function is the identity function.

While TREE may also be any sub-node within a larger tree, the returned copied
node is always a root-node."

  (define (node-copy node parent)
    ;; Make a copy of NODE (payload identical to NODE), but point the parent
    ;; link of the copy to PARENT.
    (make-node parent '() (payload (node-payload node))))

  (define (node-copy-children node copied-node)
    ;; Make copies of all child-nodes of NODE. Make these copies child-nodes of
    ;; COPIED-NODE and set the parent link of each of these copied child-nodes
    ;; to COPIED-NODE.
    (node-children-set! copied-node
                        (map (lambda (x) (node-copy x copied-node))
                             (node-children node))))

  (define (copy-with-subtrees orig-nodes copied-nodes)
    ;; Take a list of original child-nodes and a corresponding list of copied
    ;; child-nodes and call `tree-copy-internal' for each original child-node,
    ;; that is not a leaf-node, thus copying sub-trees.
    (unless (null? orig-nodes)
        (let ((cur-orig (car orig-nodes))
              (cur-copy (car copied-nodes)))
          (if (not (node-is-leaf? cur-orig))
              (tree-copy-internal cur-orig cur-copy)))
        (copy-with-subtrees (cdr orig-nodes) (cdr copied-nodes))))

  (define (tree-copy-internal orig copy)
    ;; Copy all children with subtrees of ORIG to COPY.
    (node-copy-children orig copy)
    (copy-with-subtrees (node-children orig) (node-children copy)))

  ;; Create a copy of the original root-node, copy everything and return the
  ;; copy of the root-node.
  (let ((new (node-copy tree #f)))
    (tree-copy-internal tree new)
    new))

(define* (traverse-fold proc
                        init
                        tree
                        #:key
                        (left-to-right? #t)
                        (order 'pre)
                        (payload-only? #t))
  "This function implements `fold'-like functionality for the tree data type.

It walks `tree' and calls `proc' with two arguments: The current node's payload
and the return value of the last call to `proc'. On the first call of `proc'
`init' is used as the second argument.

The function returns the proc's last return value.

The function supports a number of keyword-style arguments to alter its course
of action:

  order => One of: `pre', `post, `level'
  left-to-right? => Boolean (defaults to #t); controls the order in which
                    child-nodes are processed.
  payload-only? => Boolean (defaults to #t); controls if `proc' is
                   presented with full node objects or the node's
                   payload only."
  (define (recurse node acc)
    (traverse-fold proc acc node
                   #:left-to-right? left-to-right?
                   #:order order
                   #:payload-only? payload-only?))

  (define (children node)
    (let ((cl (node-children node)))
      (if left-to-right? cl (reverse cl))))

  (define (pick-node node)
    (if payload-only? (node-payload node) node))

  (define (recursive-traversal nodes acc)
    (if (null? nodes)
        acc
        (let ((node (car nodes)))
          (recursive-traversal (cdr nodes)
                               (if (node-is-leaf? node)
                                   (proc (pick-node node) acc)
                                   (recurse node acc))))))

  (case order
    ;; `pre' and `post' work fairly similarly, by recursing on non-leaf nodes.
    ;; `level' works by keeping a queue of nodes, that need to be processed.
    ;; Each time a new level is entered, all child-nodes of all the nodes in
    ;; the current level are added to the queue. The algorithm is finished,
    ;; when the queue runs empty.
    ((pre)
     (recursive-traversal (children tree)
                          (proc (pick-node tree) init)))
    ((post)
     (proc (pick-node tree)
           (recursive-traversal (children tree) init)))
    ((level)
     (let continue ((queue (list tree))
                    (acc init))
       (if (null? queue)
           acc
           (let ((rem (cdr queue))
                 (node (car queue)))
             (continue (append rem (children node))
                       (proc (pick-node node) acc))))))
    (else => (throw 'unknown-traversal-order order))))

(define* (traverse-map proc
                       tree
                       #:key
                       (left-to-right? #t)
                       (order 'pre)
                       (payload-only? #t))
  "Implement a `map'-like function for trees. This function calls `proc' for
each node of `tree'. It returns a *list* of proc's return values.

The function is implemented in terms of `traverse-fold' (and `cons') and
therefore accepts the same optional keyword arguments."
  (let ((return-value (list #f)))
    (traverse-fold (lambda (elem acc)
                     (set-cdr! acc (list (proc elem)))
                     (cdr acc))
                   return-value
                   tree
                   #:left-to-right? left-to-right?
                   #:order order
                   #:payload-only? payload-only?)
    (cdr return-value)))

(define* (traverse-filter proc
                          tree
                          #:key
                          (left-to-right? #t)
                          (order 'pre)
                          (payload-only? #t))
  "Traverse TREE and call PROC on each payload/node. Return a list of
payload/nodes for which proc returned non-#f.

The function is implemented in terms of `traverse-fold' and therefore accepts
the same optional keyword arguments."
  (let ((return-value (list #f)))
    (traverse-fold (lambda (elem acc)
                     (if (proc elem)
                         (begin (set-cdr! acc (list elem))
                                (cdr acc))
                         acc))
                   return-value
                   tree
                   #:left-to-right? left-to-right?
                   #:order order
                   #:payload-only? payload-only?)
    (cdr return-value)))

(define* (traverse-search proc
                          tree
                          #:key
                          (left-to-right? #t)
                          (order 'pre)
                          (payload-only? #t))
  "Traverse TREE and call PROC on each payload/node. Return a list with the
first payload/node for which proc returned non-#f being the only item in it. If
PROC never returned non-#f, the empty list is returned.

The function is implemented in terms of `traverse-map' and therefore accepts
the same optional keyword arguments."
  (call/ec (lambda (escape-with)
             (traverse-map (lambda (elem)
                             (if (proc elem)
                                 (escape-with (list elem))))
                           tree
                           #:left-to-right? left-to-right?
                           #:order order
                           #:payload-only? payload-only?)
             '())))

(define* (flatten tree
                  #:key
                  (order 'pre)
                  (left-to-right? #t))
  "Flatten all payload objects of `tree' into a list.

This function is implemted in terms of `traverse-map' and thus `traverse-fold'
(and the identity function) and therefore supports the same optional keyword
arguments (except for `payload-only?', which it sets to #t.)."
  (traverse-map (lambda (x) x)
                         tree
                         #:left-to-right? left-to-right?
                         #:order order
                         #:payload-only? #t))
