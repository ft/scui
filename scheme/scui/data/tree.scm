;; Copyright (c) 2019 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

(define-module (scui data tree)
  #:use-module (ice-9 control)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (scui data zipper)
  #:export (make-tree
            make-node
            leaf-node
            node?
            n:payload
            n:nodes
            leaf-node?
            tree-copy
            tree-map
            tree-fold
            tree-merge
            tree->nlist))

(define-immutable-record-type <node>
  (make-node payload nodes)
  node?
  (payload n:payload)
  (nodes n:nodes))

(define (leaf-node? node)
  "Return #t of ‘node’ is a leaf-node.

The argument supplied has to be a node object."
  (null? (n:nodes node)))

(define (leaf-node payload)
  "Creates a leaf-node with a given ‘payload’."
  (make-node payload '()))

(define (tree-map fnc tree)
  "Apply ‘fnc’ to each element in ‘tree’ and return a tree of the same
structure, where payload items are populated with the return values of that
function application."
  (let ((data (fnc (n:payload tree))))
    (if (leaf-node? tree)
        (leaf-node data)
        (make-node data (map (lambda (t) (tree-map fnc t))
                             (n:nodes tree))))))

(define (tf:pre fnc init tree ltr?)
  (if (leaf-node? tree)
      (fnc tree init)
      (fold (lambda (node acc) (tf:pre fnc acc node ltr?))
            (fnc tree init)
            ((if ltr? identity reverse) (n:nodes tree)))))

(define (tf:post fnc init tree ltr?)
  (if (leaf-node? tree)
      (fnc tree init)
      (fnc tree
           (fold (lambda (node acc) (tf:post fnc acc node ltr?))
                 init
                 ((if ltr? identity reverse) (n:nodes tree))))))

(define (tf:level fnc init tree ltr?)
  (let next ((q (list tree)) (acc init))
    (if (null? q)
        acc
        (let ((rest (cdr q))
              (node (car q)))
          (next (append rest ((if ltr? identity reverse) (n:nodes node)))
                (fnc node acc))))))

(define* (tree-fold fnc init tree
                    #:key (left-to-right? #t) (order 'pre))
  ((case order
     ((pre) tf:pre)
     ((post) tf:post)
     ((level) tf:level)
     (else (throw 'unknown-tree-traversal order)))
   fnc init tree left-to-right?))

(define (tree-merge p-cons n-cons state t1 t2)
  (make-node (p-cons state (n:payload t1) (n:payload t2))
             (let loop ((t1s (n:nodes t1))
                        (t2s (n:nodes t2))
                        (state state)
                        (acc '()))
               (cond ((null? t1s) (reverse acc))
                     ((null? t2s) (reverse acc))
                     (else (let ((n-state (n-cons state
                                                  (n:payload (car t1s))
                                                  (n:payload (car t2s)))))
                             (loop (cdr t1s) (cdr t2s) n-state
                                   (cons (tree-merge p-cons n-cons state
                                                     (car t1s) (car t2s))
                                         acc))))))))

(define (tree-copy tree)
  (make-node (n:payload tree) (map tree-copy (n:nodes tree))))

(define (tree->nlist tree)
  "Convert ‘tree’ to nested list of payloads."
  (if (leaf-node? tree)
      (n:payload tree)
      (list (n:payload tree)
            (map tree->nlist (n:nodes tree)))))

(define *with-tree-debug* #f)

(define (tree-debug x)
  (when *with-tree-debug*
    (format #t "DEBUG: ~a~%" (syntax->datum x))))

(define-syntax expand-tree-node
  (lambda (x)
    (tree-debug x)
    (syntax-case x (! ~)
      ((_ (! pl)) #'(leaf-node pl))
      ((_ (~ e))
       (error "make-tree: (~ payload ...) without node list. Use (! payload) instead!"))
      ((_ (~ pl (ns ...))) #'(make-tree pl (ns ...)))
      ((_ exp) #'(let ((evd exp))
                   (if (node? evd) evd
                       (throw 'not-a-node evd 'exp)))))))

(define-syntax make-tree
  (lambda (x)
    (tree-debug x)
    (syntax-case x (! ~)
      ((_ pl) (error "Cannot use make-tree with just payload!"))
      ((_ (! e ...) exp ...)
       (error "Cannot use ! inside payload expressions!"))
      ((_ (~ e ...) exp ...)
       (error "Cannot use ~ inside payload expressions!"))
      ((_ pl (! e ...) ...)
       (error "Cannot use ! outside of a list expression!"))
      ((_ pl (~ e ...) ...)
       (error "Cannot use ~ outside of a list expression!"))
      ((_ pl (ns ...)) #'(make-node pl (list (expand-tree-node ns) ...)))
      ((_ pl exp0 exps ...)
       (error "make-tree error, use: (make-tree payload (nodes...))")))))

(define (depth-first* fnc tree)
  (or (fnc tree)
      (make-node (n:payload tree)
                 (map-in-order (lambda (node)
                                 (depth-first* fnc node))
                               (n:nodes tree)))))

(define (minimap fnc lst)
  (if (null? lst) lst
      (let* ((head (car lst))
             (rest (cdr lst))
             (fnc:head (fnc head))
             (fnc:rest (minimap fnc rest)))
        (if (and (eq? fnc:head head)
                 (eq? fnc:rest rest))
            lst
            (cons fnc:head fnc:rest)))))

(define (depth-first fnc tree)
  (or (fnc tree)
      (let ((kids (minimap (lambda (node)
                             (depth-first fnc node))
                           (n:nodes tree))))
        (if (eq? kids (n:nodes tree))
            tree
            (make-node (n:payload tree) kids)))))

(define (tree-zip tree)
  (reset (depth-first (lambda (t)
                        (shift f (make-zipper t f)))
                      tree)))

(define (tree-select fnc tree)
  (let loop ((cursor (tree-zip tree)))
    (and (zipper? cursor)
         (if (fnc (z:node cursor))
             cursor
             (loop ((z:cont cursor) #f))))))

(define (make-counter-to n)
  (let ((i 0))
    (lambda (_)
      (cond ((= i n) #t)
            (else (set! i (+ 1 i))
                  #f)))))

(define (tree-select-n n tree)
  (tree-select (make-counter-to n) tree))
