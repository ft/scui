;; Copyright (c) 2014 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

(define-module (scui data window)
  #:use-module (srfi srfi-9)
  #:export (make-ui/window
            ui/window?
            uie))

(define-record-type <ui/window>
  (make-ui/window* window
                   window-cfg
                   focus-handler
                   unfocus-handler
                   redraw-handler
                   spec-hint
                   usable?
                   height
                   width
                   cx
                   cy)
  ui/window?
  ;; Link to the actual ncurses window. (For testing purposes, this may also be
  ;; a symbol.)
  (window uiw/get-window)
  ;; The ncurses window will be allocated automatically when it is needed for
  ;; the first time. This is a callback that can be used to preform any desired
  ;; configuration steps, like setting keypad!.
  (window-cfg uiw/window-cfg)
  ;; A function, that is called when the window get focused. This should take
  ;; care of setting up the focused key-map and any visual feedback, if any. If
  ;; set to #f, the window cannot be focused (think: title-bar windows).
  (focus-handler uiw/get-focus-handler)
  ;; A function, that is called when the window loses focus. It should reset
  ;; any visual feedback to the non-focused state. If set to #f, it is ignored.
  (unfocus-handler uiw/get-unfocus-handler)
  ;; A function called when the window needs to be redrawn.
  (redraw-handler uiw/get-redraw-handler)
  ;; This is either a function or an integer, that tells the layout which size
  ;; to use (horizontally or vertically, depending on the type of split) in
  ;; case the layout does not define such a value on it's own.
  (spec-hint uiw/get-spec-hint uiw/set-spec-hint!)
  ;; This is either a function or the symbol #t. If it's #t, the window
  ;; signals, that it is usuable no matter what its dimensions are. If it's a
  ;; function, that function shall either return #f or #t. The function is
  ;; presented with the windows calculated width and height.
  (usable? uiw/usable?)
  ;; Height and width integers (or #f if unset).
  (height uiw/get-height uiw/set-height!)
  (width uiw/get-width uiw/set-width!)
  ;; Coordinate integers (starting at [0,0] which is the top left corner), or
  ;; #f if unset.
  (cx uiw/get-x uiw/set-x!)
  (cy uiw/gety uiw/set-y!))

(define (default-window-cfg-blocking win)
  (keypad! win))

(define (default-window-cfg-non-blocking win)
  (keypad! win)
  (nodelay! win))

(define (default-window-cfg terminal)
  (if (uit/non-blocking? terminal)
      default-window-cfg-non-blocking
      default-window-cfg-blocking))

(define* (make-ui/window #:key
                         (window-cfg #f)
                         (focus #f)
                         (unfocus #f)
                         (redraw #f)
                         (spec-hint #f)
                         (usable? #t)
                         (height #f)
                         (width #f)
                         (x #f)
                         (y #f))
  (make-ui/window* #f
                   #f
                   focus
                   unfocus
                   redraw
                   (or spec-hint 'fill)
                   usable?
                   height width
                   x y))

(define (uiw/initialise window)
  #t)
