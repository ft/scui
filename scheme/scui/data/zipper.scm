;; Copyright (c) 2019 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

(define-module (scui data zipper)
  #:use-module (srfi srfi-9 gnu)
  #:export (<zipper>
            make-zipper
            zipper?
            z:cont
            z:node
            zip-up))

;; The code  that modifies data in  this scui's tree implementation  as well as
;; purely functional lists is based on  the zipper idea from Gérard Huet, 1997:
;; "Functional Pearl: The  Zipper" . Journal of Functional  Programming. 7 (5):
;; 549–554;  with  additional  implementation   ideas  from  Oleg  Kiselyov  in
;; 7eb8ac3e.0410241519.3f2b3e50@posting.google.com, to  comp.lang.scheme, where
;; he suggests the use delimited  continuations to implement zippers. And since
;; GNU Guile implements that kind of continuation, that is the path this module
;; will take.

(define-immutable-record-type <zipper>
  (make-zipper current-node continuation)
  zipper?
  (current-node z:node)
  (continuation z:cont))

(define (zip-up zipper)
  (if (zipper? zipper)
      (zip-up ((z:cont zipper)
               (z:node zipper)))
      zipper))
