;; Copyright (c) 2013 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

;; An element in scui term is an ncurses window with some bits of additional
;; information that is required to make it useful in layouts and the input
;; model.


(define-module (scui element)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 optargs)
  #:export (ui/element?
            make-ui/element
            uie/cx
            uie/cx-set!
            uie/cy
            uie/cy-set!
            uie/focus-handler
            uie/height
            uie/height-set!
            uie/redraw-handler
            uie/spec-hint
            uie/spec-hint-set!
            uie/unfocus-handler
            uie/width
            uie/width-set!
            uie/window))

(define-record-type <ui/element>
  (make-ui/element* window
                    focus-handler
                    unfocus-handler
                    redraw-handler
                    spec-hint
                    usable?
                    height
                    width
                    cx
                    cy)
  ui/element?
  ;; Link to the actual ncurses window. (For testing purposes, this may also be
  ;; a symbol.)
  (window uie/window)
  ;; A function, that is called when the element get focused. This should take
  ;; care of setting up the focused key-map and any visual feedback, if any. If
  ;; set to #f, the window cannot be focused (think: title-bar windows).
  (focus-handler uie/focus-handler)
  ;; A function, that is called when the element loses focus. It should reset
  ;; any visual feedback to the non-focused state. If set to #f, it is ignored.
  (unfocus-handler uie/unfocus-handler)
  ;; A function called when the element needs to be redrawn.
  (redraw-handler uie/redraw-handler)
  ;; This is either a function or an integer, that tells the layout which size
  ;; to use (horizontally or vertically, depending on the type of split) in
  ;; case the layout does not define such a value on it's own.
  (spec-hint uie/spec-hint uie/spec-hint-set!)
  ;; This is either a function or the symbol #t. If it's #t, the element
  ;; signals, that it is usuable no matter what its dimensions are. If it's a
  ;; function, that function shall either return #f or #t. The function is
  ;; presented with the windows calculated width and height.
  (usable? uei/usable?)
  ;; Height and width integers (or #f if unset).
  (height uie/height uie/height-set!)
  (width uie/width uie/width-set!)
  ;; Coordinate integers (starting at [0,0] which is the top left corner), or
  ;; #f if unset.
  (cx uie/cx uie/cx-set!)
  (cy uie/cy uie/cy-set!))

(define* (make-ui/element #:key
                          (window 'dummy)
                          (focus-handler #f)
                          (unfocus-handler #f)
                          (redraw-handler #f)
                          (spec-hint #f)
                          (usable? #t)
                          (height #f)
                          (width #f)
                          (cx #f)
                          (cy #f))
  (make-ui/element* window
                    focus-handler unfocus-handler redraw-handler
                    (or spec-hint 'fill) usable? height width
                    cx cy))
