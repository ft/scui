;; Copyright (c) 2013 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

;; While (scui key) provides  a way to turn raw input data  from ncurses into a
;; coherent  data  structure, that  contains  all  available information,  this
;; module implements a way to table keys to certain actions.
;;
;; There are  a few things to  consider (this is  a kind of "big  picture" with
;; respect to key-bindings, which goes beyond the scope of this module):
;;
;;  - There should be two key-table name-spaces: One for application developers
;;    and  one  for  application  user.  That way,  users  can  add  their  own
;;    key-tables  without having  to bother  with checking  if they  clash with
;;    built-in tables.  Conceptually, there is  no difference between  the two;
;;    it's just separate  name-spaces. (See the bullet  point about arbitrarily
;;    long key-sequences below as well.)
;;
;;  - Character-like keys and special  keys might share the same numeric value,
;;    so there  needs to be  one table for `character'  keys and one  table for
;;    `special' and `user' keys.
;;
;;  - What to do with  keys that arrive  but aren't bound to  anything? First:
;;    Allow a  keytable to define a  catch-all function. If that  still doesn't
;;    consume the key, use a fall-back  keytable, that has its own (potentially
;;    different)  catch-all function.  For example,  in a  keytable for  a line
;;    editor  you   might  want  all   character-like  keys  to  be   bound  to
;;    `self-insert' and all special-keys to be bound to `ignore-key'. ...except
;;    for those that are bound explicitly.
;;
;;  - What "things" should  be bindable to  keys? Thunks (ie.  functions, that
;;    take no arguments at all)  are obvious candidates. Another possibility is
;;    functions that take  one argument; that would be set  to the "interactive
;;    environment" whenever the function is triggered.
;;
;;  - The "interactive environment"? I'm glad you asked: With applications like
;;    emacs  or vi,  you are  able to  modify the  behaviour of  a keypress  by
;;    pressing  prefix-argument  keys  in  front  of  them.  In  emacs,  that's
;;    M-<number>  or C-u  etc. In  vi,  it's an  integer prefix  like "123"  in
;;    normal-mode.  Without   bias  towards   any  of  these   solutions,  scui
;;    acknowledges that the basic idea is sound. In order to support this, keys
;;    should   be   bindable    to   functions   like   `universal-prefix-arg',
;;    `push-self-to-prefix-arg',  or `push-self-to-numeric-prefix-arg'.  Prefix
;;    arguments like  that should  be part of  the interactive  environment, so
;;    that the  receiving function can  inspect it and behave  accordingly. The
;;    key that  triggered the function  itself should  also be linked  from the
;;    interactive  environment,  so  the   same  function  can  potentially  do
;;    different things, when it is bound to different keys.
;;
;;  - How to  handle arbitrarily long input sequences? For  example, in a music
;;    playing application, the user might want to be able to press "C-x f 2" to
;;    skip  forward by  two minutes  and "C-x  b 2"  to skip  backwards by  two
;;    minutes. There are several ways to go about this, but here is how scui is
;;    going to do  it: Allow the user to add  user-defined key-tables, and then
;;    be able to  "latch" a key-table. To "latch" a  key-table means: To lookup
;;    the very next key-press in the key-table that was "latched".
;;
;;    So, you'd bind "C-x" to  "latch-user-table ctrl-x-table" in the base key-
;;    table.  Then   in  "ctrl-x-table"   you'd  bind   "latch-user-table  for-
;;    ward-skip-table" to  the "f" key  and in that  table you'd then  have the
;;    digit-keys bound to skip actions in that "forward-skip-table" key-table.
;;
;; "This sounds all very complex!" - It's not so bad. Let me break it down once
;; again:
;;
;;  - A key comes in. It is looked up in the current active keytable.
;;
;;  - If it matches, great, then the corresponding action is carried out.
;;
;;  - If not, a catch-all  function for the  table is called, if  the keytable
;;    defines it. The  purpose is to table many bindings  to a similar purpose,
;;    like ignoring all keys that couldn't be looked up etc.
;;
;;  - If the  catch-all function didn't take care of the  key, it falls through
;;    to a  global fallback key-table. With  that key-table, the same  dance is
;;    danced again: If the key matches, great! If not, a catch-all function, if
;;    it's defined, is triggered to take care of the key.
;;
;;  - If the fallback key-table's catch-all  function does not consume the key,
;;    it is finally ignored.
;;
;; Bindable functions are  either thunks (functions that  don't take arguments)
;; or functions of one  argument, in which case that argument is  set to a data
;; structure that describes the current interactive state (prefix arguments and
;; stuff like that).
;;
;; And that is it, basically. Beyond  that, user's are allowed to defined their
;; own keytables,  switch to them  or only latch them  for the next  key press,
;; which enables arbitrarily long multi-key bindings.

(define-module (scui key-table)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 optargs)
  #:use-module (scui key)
  #:export (add-table
            get-table
            bind-code
            lookup-key))

(define-record-type <key-table>
  (make-key-table* character-bindings
                 special-key-bindings
                 catch-all-function)
  key-table?
  (character-bindings key-table-character-bindings)
  (special-key-bindings key-table-special-key-bindings)
  (catch-all-function key-table-catch-all-function))

(define (make-key-table catch-all-function)
  (make-key-table* (make-hash-table)
                   (make-hash-table)
                   catch-all-function))

(define application-key-tables (make-hash-table))
(define user-key-tables (make-hash-table))

(define (add-table name user-table? catch-all-function)
  "Add a new <key-table> named NAME to the system's set of key-tables. If
USER-TABLE? is true, add it to the separate name-space for user-defined
key-tables instead. Set the table's catch-all function to CATCH-ALL-FUNCTION.
Return the newly created table if the table was added or #f if a table by the
name already exists.

NAME has to be a symbol."
  (if (get-table name user-table?)
      #f
      (let ((new (make-key-table catch-all-function)))
        (hashq-set! (if user-table? user-key-tables application-key-tables)
                    name
                    new)
        new)))

(define (get-table name user-table?)
  "Get a <key-table> named NAME from the system's set of key-tables. If
USER-TABLE? is true, the table is taken from the separate name-space for
user-defined key-tables instead. If a table named NAME cannot be found, #f is returned instead."
  (hashq-ref (if user-table? user-key-tables application-key-tables) name))

(define (bind-code code special? key-table action)
  "Adds a binding for a key described by CODE to KEY-TABLE pointing to the
desired ACTION. If SPECIAL? is true, add to the special-keys part of the
key-table, otherwise add to the ordinary key-table."
  (let ((table-getter (if special?
                          key-table-special-key-bindings
                          key-table-character-bindings)))
    (hashv-set! (table-getter key-table) code action)))

(define (lookup-key* key key-table)
  "See if there is a binding for KEY in KEY-TABLE and return it. If there is
no such binding, return #f."
  (let* ((type (key-type key))
         (table-getter (if (or (eq? type 'special)
                               (eq? type 'user))
                           key-table-special-key-bindings
                           key-table-character-bindings)))
    (hashv-ref (table-getter key-table) (key-code-point key))))

(define (catch-all key key-table)
  "If KEY-TABLE defines a catch-all function, call it with KEY as its
argument and return the value obtained from that. Otherwise, return #f."
  (let ((ca (key-table-catch-all-function key-table)))
    (if (procedure? ca)
        (ca key)
        #f)))

(define (lookup-key key primary-table fallback-table)
  "Take a <key> instance pointed to by KEY and perform a complete lookup,
by first checking PRIMARY-TABLE (first the tablepings themselves, then the
catch-all function) and if that comes up empty checking FALLBACK-TABLE.

Returns the tableped action or #f in case then key did not match anything."
  (or (and (key-table? primary-table)
           (or (lookup-key* key primary-table)
               (catch-all key primary-table)))
      (and (key-table? fallback-table)
           (or (lookup-key* key fallback-table)
               (catch-all key fallback-table)))))
