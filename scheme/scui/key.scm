;; Copyright (c) 2013 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

;; This module defines a container for keys a user might press in a scui
;; application. I contains all information to drive the user-input loop, that
;; reacts to key-presses.
;;
;; With respect to the `meta?' entry: Keyboard handling in unix terminals is
;; hard. The meta-key (often mapped the the Alt key on PC keyboards) exhibits
;; different, oftentimes configurable behaviour. In some terminals, it's like
;; another shift-key that will cause the terminal to emit different characters
;; (often the ones with the MSB of an 8-bit character set - or a unicode
;; representation of that...). In other terminals, a meta-key combination will
;; cause the terminal to send an escape character before the unchanged
;; character that was pressed with meta.
;;
;; The latter is the way scui understands meta-combinations (the other
;; behaviour would not require any different handling anyway, since it's just
;; another character with its raw data and code-point). Here is how the
;; machinery handles things:
;;
;;  - If a character, that's not the ASCII escape character is seen in the
;;    input, it's handed through without setting `ignore?' or `meta?'
;;
;;  - When an escape character is first seen, it's noted internally and the key
;;    is handed to the machinery with the `ignore?' bit set, which will cause
;;    the rest of the code to ignore the key while still notifying it, that an
;;    escape character is pending.
;;
;;  - The next character that arrives gets its `meta?' bit set; the internal
;;    "escape-pending" bit is cleared and the key is handed to the rest of the
;;    code.
;;
;; The distinction in `type' along with `raw' and `code-point' is because of
;; how characters are handled in the ncurses library. The following values are
;; possible with `type:
;;
;;  - character: The input was an actual character. Could be more than one
;;               byte, depending on the active locale. `raw' will be exactly
;;               that character and `code-point' is (char->integer raw). Say we
;;               have (with an UTF-8 locale) a character as the input, that
;;               consists of two bytes "0xc3 0xa4", which (in Guile notation)
;;               is #\ä (this is put into `raw'). Is character is represented
;;               by the unicode code-point U+00C4 (this is put as #xc4 which is
;;               the decimal integer 196 into `code-point').
;;
;;  - special: The second thing ncurses does is map certain byte-sequences to
;;             that are emitted by special keys (like the HOME key) to
;;             integers. In this case `raw' and `code-point' will be the same.
;;
;;  - user: Technically, this type is not different from `special'. This type
;;          is based on the `define_key(3)' feature of ncurses. What you can do
;;          with this is map *any* byte sequence you like to an integer. With
;;          that, you can support certain extended key-combinations that are
;;          not advertised via the terminal's termcap/terminfo database entry.
;;          The feature is implemented in "user-key.scm".
;;
;;  - invalid: If type is set to this, the input data was nonsensical and could
;;             not be categorised by `process-input' at all.
;;
;; Finally, to give an idea of how this fits into the whole scheme of things:
;; You get user-input via ncurses' `getch' function. That user-input is what is
;; fed into `process-input'. And `process-input' looks at the input and returns
;; an instance of <key>. These instances are then handled by more high-level
;; code to eg. look up key bindings in key maps.

(define-module (scui key)
  #:use-module (srfi srfi-9)
  #:use-module (ncurses curses)
  #:use-module (scui user-key)
  #:export (make-key
            key?
            key-raw
            key-code-point
            key-type
            key-ignore?
            key-meta?
            key-is-7bit?
            printable-key
            process-input))

(define-record-type <key>
  (make-key raw code-point type meta? ignore?)
  key?
  (raw key-raw)
  (code-point key-code-point)
  (type key-type)
  (meta? key-meta?)
  (ignore? key-ignore?))

(define (key-is-7bit? key)
  (let ((cp (key-code-point key)))
    (and (positive? cp)
         (<= cp 127))))

(define got-escape #f)
(define code-point-escape (char->integer #\esc))
(define invalid-key-tag "(invalid key)")

(define (key->type key)
  "Return type for input data specified by KEY."
  (cond ((char? key) 'character)
        ((and (integer? key)
              (positive? key))
         (if (key-is-user-key? key) 'user 'special))
        (else 'invalid)))

(define (process-input input)
  "Inspect input data INPUT, fill a <key> instance and return it.

This procedure is used to post-process raw data from ncurses' `getch'
facility. It supports normal character data, special keys, additional
user-defined special keys as well as unicode input data, should the active
locale support that."
  (cond ((and (not got-escape)
              (eq? input #\esc))
         (set! got-escape #t)
         (make-key input code-point-escape 'character #f #t))
        (else
         (let ((meta? got-escape))
           (if got-escape (set! got-escape #f))
           (make-key input
                     (if (char? input) (char->integer input) input)
                     (key->type input)
                     meta?
                     #f)))))

(define (special-keyname-strip name)
  "Strip the KEY_ prefix, that ncurses' `keyname' function returns for
special-keys."
  (if (string-prefix? "KEY_" name)
      (substring name 4)
      name))

(define (printable-key key)
  "Return a printable name for KEY, a <key> instance."
  (let ((type (key-type key))
        (code (key-code-point key)))
    (cond ((eq? type 'character)
           (if (< code 128)
               (keyname code)
               (string (key-raw key))))
          ((eq? type 'special)
           (special-keyname-strip (keyname code)))
          ((eq? type 'user)
           (get-user-key-name code))
          (else invalid-key-tag))))
