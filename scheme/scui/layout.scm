;; Copyright (c) 2013 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

;; A layout describes how to split the screen and assemble a subset of all
;; windows in the resulting compartments. These properties map fairly directly
;; to a tree structure, where leaf nodes are the actual windows and non-leaf
;; nodes describe a way to compartmentalise (a part of) the screen (ie. a
;; split).
;;
;; Since a layout doesn't necessarily use all windows that the entire program
;; actually runs, there is an operation to figure out the set of currently
;; visible windows:
;;
;;      (determine-active-set layout)
;;        => list
;;
;; Exactly *one* of the windows from that list has the keyboard focus.
;;
;; There are two kinds of splits: Vertical ones and horizontal ones. There are
;; different ways to define these, resulting in opposite meanings of the two
;; terms. And this is happening in the wild already (just compare how vim,
;; emacs and tmux define these terms). Here is how scui defines the meanings:
;;
;;  - Vertical split: A part of the screen is split into two or more parts and
;;    the resulting windows are aligned vertically.
;;
;;  - Horizontal split: A part of the screen is split into two or more parts
;;    and the resulting windows are aligned horizontally.
;;
;; Here is a fairly simple example of a split screen.
;;
;;  +------------------------------------+---------------------------------+
;;  |                                    |                                 |
;;  |                                    |                                 |
;;  |                                    |                                 |
;;  |               comp-a               |               comp-b            |
;;  |                                    |                                 |
;;  |                                    |                                 |
;;  |                                    |                                 |
;;  +------------------------------------+---------------------------------+
;;  |                                                                      |
;;  |                                                                      |
;;  |                                                                      |
;;  |                                  comp-c                              |
;;  |                                                                      |
;;  |                                                                      |
;;  |                                                                      |
;;  +----------------------------------------------------------------------+
;;
;; The first split that is done here is a vertical split, resulting in the
;; upper part and the compartment occupied by "comp-c". Then a horizontal split
;; is performed on the upper area, resulting in the compartments for "comp-a"
;; and "comp-b".
;;
;; So a first naive way of defining the layout would be:
;;
;;    (make-layout
;;      (vertical-split
;;        (horizontal-split 'comp-a 'comp-b)
;;        'comp-c))
;;
;; Obviously, the symbols would need to be exchanged with actual window
;; content. And then, you might want to be able to do splits, that assign
;; certain sizes for some windows (the above example just divides space into
;; equally sized compartments).
;;
;; And sizes make things complicated again, especially if you take into account
;; that a window might want to constrain its width and/or height to a certain
;; value.
;;
;; So, how are the sizes determined?
;;
;;   A layout may define a window's size. The dimension described by the size
;;   parameter depends on the type of split in question. E.g. in a vertical
;;   split, the size parameter describes a window's vertical size. Integer
;;   values describe the exact size in either lines or columns. Non-integer
;;   values between 0 and 1 describe size in fractions of the parent
;;   compartments size.
;;
;;   A window itself may define fixed or minimum dimensions. For example, a
;;   title-window would likely define a fixed hight of a single line. This
;;   value always wins against any other source that might define a dimension.
;;
;;   This has several implications: 1. If a compartment has an available height
;;   of 15 lines, but a window requires at least 20 lines, the layout in
;;   question is not usable with the current screen size. 2. If you define a
;;   horizontal split and use two windows in there, that have different fixed
;;   heights defined, that layout cannot be used either.
;;
;;   Each split definition may have exactly one window, that uses a size
;;   definition of the symbol `fill'. Such a window will consume all remaining
;;   space in the direction determined by the type of split it is used in.
;;
;;   To always fill the available screen completely, each split should define
;;   such a compartment. Even with fractional sizes. Fractional sizes always
;;   round size down to the next lower number of lines/columns. So, with two
;;   windows both using 0.5 for their size will leave one empy line/column if
;;   the compartment they fill has an odd number of lines/columns.
;;
;;   To summarise, sizes are determined like this:
;;
;;     - Does a window define a fixed size?
;;       -> This size is used. All other windows must be okay with that size.
;;
;;     - Does a window define a minimum size?
;;       -> The compartment has to be able to satisfy that requirement.
;;
;;     - If a window does not have any specific requirements as to size, ask
;;       the layout for guidance: The #:size parameter.
;;
;; That leaves the question of how to design a DSL that is able to concisely
;; express such layouts. For one, you'd want to compose a layout on the spot.
;; And the simplest of all layouts is a layout that contains just one window.
;;
;;   (make-layout (window window-cb))
;;
;; ...where `window-cb' is an instance of a container, that implements all
;; functionality of a window. Now for more complex layouts:
;;
;;   (make-layout
;;     (vertical-split
;;       (horizontal-split
;;         (window win-a-cb)
;;         (window win-b-cb))
;;       (horizontal-split
;;         #:with-separators
;;         (window win-c1-cb #:size (/ 1 3))
;;         (window win-c2-cb))
;;       (window win-d-cb)
;;       (window win-e-cb)))
;;
;; And then, you might want to change the layout at will: Move windows around.
;; Introduce a new split where before there only was a window. Add an
;; additional window to an existing split. Remove a window from a split.

(define-module (scui layout)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 optargs)
  #:use-module (scui element)
  #:use-module (scui data tree)
  #:export (make-layout
            vertical
            horizontal
            window
            layout-init))

(define-record-type <ui/terminal>
  (make-ui/terminal type width height)
  ui/terminal?
  (type uit/get-type)
  (width uit/get-width uit/set-width!)
  (height uit/get-height uit/set-height!))

;; A layout is a tree - from (scui data tree). Non-leaf nodes describe splits,
;; and this is the data-type that's the payload for such non-leaf nodes.
(define-record-type <ui/split>
  (make-ui/split type x-size y-size options)
  ui/split?
  (x-size uis/x-size)
  (y-size uis/y-size)
  (type uis/type)
  (options uis/options))

(define (new-split type . items)
  (make-tree #:payload (make-ui/split type #f #f '())
             #:children (map (lambda (x)
                               (if (node? x)
                                   x
                                   (make-leaf-node x)))
                             items)))

(define (vertical . items)
  (apply new-split 'vertical items))

(define (horizontal . items)
  (apply new-split 'horizontal items))

(define (make-layout root)
  (cond ((tree? root) root)
        ((ui/element? root) (make-tree #:payload root))
        (else (throw 'unknown-root-in-layout root))))

(define (element-demand elem extract)
  (let* ((hint (uie/size-hint elem))
         (this (extract hint))
         (wish (car this)))
    (if (integer? wish)
        this
        (cons '* (cdr this)))))

(define (element-width-demand elem)
  (element-demand elem car))

(define (element-height-demand elem)
  (element-demand elem cadr))

(define (demand-width x)
  (cond ((ui/element? x)
         (hint->demand (uie/size-hint x)))
        ((ui/split? x)
         (cond ((eq? (uis/type x) 'horizontal)
                1)
               ((eq? (uis/type x) 'vertical)
                1)
               (else (throw 'scui/unknown-split-type x))))
        (else (throw 'scui/unknown-payload x))))

;; Validation:
;;
;;  1. All leaf-nodes contain ui-elements.
;;  2. All non-leafs contain ui-splits.
;;  3. In splits:
;;     a. height constraints of children may not conflict.
;;     b. width constraints of children may not conflict.
;;
;; Strategy:
;;
;;  1. Calculate demand:
;;     a. width
;;     b. height
;;
;;     → Can be an integer, the symbol * or a list that forms a special
;;       kind of sum:
;;         - integers demand a fixed amount of space.
;;         - Anything, that does not demand a fixed amount of space
;;           falls back to demanding a minimal value, that defaults to 1.
;;         - The symbol * denotes that the parameter is not fixed.
;;
;;     This you can ask of any node in a system, split or element and its
;;     calculation depends on the kind of split that is used.
;;
;;     Perform this calculation from leafs towards the root. Now each split
;;     knows the demand of columns and rows of its children.
;;
(define (apply-layout layout terminal)
  "With parameters from TERMINAL, recalculate the entire LAYOUT."
  (let ((width (uit/get-width terminal))
        (height (uit/get-height terminal)))
    (traverse-fold trickle-down-sizes
                   (cons width heigth)
                   layout)))

;;; ----------------------
;;; --- OLD CODE BELOW ---
;;; ----------------------

;;(define (new-split type args)
;;  (make-ui/split type #f #f
;;                 (fold (lambda (x acc)
;;                         (if (keyword? x)
;;                             (cons x acc)
;;                             acc))
;;                       '()
;;                       args)))
;;
;;(define (split-backend type args)
;;  (let ((new (make-tree #:payload (new-split type args))))
;;    (for-each (lambda (x)
;;                (cond ((ui/element? x)
;;                       (node-cons! (make-tree #:payload x) new))
;;                      ((tree? x)
;;                       (node-cons! x new))))
;;              (reverse args))
;;    new))
;;
;;(define (vertical-split . args)
;;  (split-backend 'vertical args))
;;
;;(define (horizontal-split . args)
;;  (split-backend 'horizontal args))
;;
;;(define* (window win #:key (size #f))
;;  (if size (uie/spec-hint-set! win size))
;;  win)
;;
;;
;;(define (layout-is-trivial? layout)
;;  (node-is-leaf? layout))
;;
;;(define (layout-init layout width height)
;;  (if (layout-is-trivial? layout)
;;      (let ((window (node-payload layout)))
;;        (uie/width-set! window width)
;;        (uie/height-set! window height))))
