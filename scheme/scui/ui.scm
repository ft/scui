;; Copyright (c) 2013 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

;; This is the top-level module of `scui'. If contains functionality that
;; application writers will most likely use in their initialisation routines as
;; well as their main loops.
;;
;; In here, you will find curses-initialisation routines, the top-level key
;; handling machinery, book-keeping (like "Which key-table is currently
;; active?", "Which layout is currently active?") etc.

(define-module (scui ui)
  #:use-module (ncurses curses)
  #:use-module (scui key-table)
  #:export (exit-curses
            suspend-curses))

(define curses-standard-screen #f)
(define current-key-table (add-table 'default #f identity))
(define latched-key-table #f)

(define (handle-input input ignored-handler)
  "Do the whole give-me-a-key-i'll-give-you-an-action dance.

INPUT is the key you want handled, IGNORED-HANDLER is a procedure of one
argument, that is called when an <key> with the `ignored?' bit set comes in.

This is the function you want to use, if `wait-for-input' does not fit into
your program's main-loop model. You might want to see to handling single escape
keys."
  #f)

(define (wait-for-input)
  "This is the one-size-fits-all approach to keyboard handling. It's supposed
to be part of a program's main-loop.

Here is what it does:

  - Read a key from the keyboard.

  - If it's the escape key with the `ignored?' bit set, set a
    short timeout. If that timeout runs out with another key
    coming in, handle this escape key as a single escape key.

  - Lookup incoming keys using `handle-input' and execute any
    actions that process yields.

  - If no keys are coming in, timeout after a short while, to
    get the rest of the main-loop some time to handle other
    responsibilities.

Both timeouts (the short one for escape and the longer usual one are
configurable."
  #f)
