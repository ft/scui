;; Copyright (c) 2013 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

;; Implementation of user-definable keys in the realm of ncurses via its
;; `define_key(3)' facility.
;;
;; There are at least three cases in which this makes sense:
;;
;;  1. The terminal definition (termcap or terminfo) is broken or outdated and
;;     an escape sequence, that is send by a certain key, is not recognised
;;     automatically. `define-key' can be used to work around such breakage.
;;
;;  2. The actual terminal runs a multiplexer (such as GNU screen or tmux),
;;     that uses a terminal definition (like TERM=screen), that does not
;;     recognise a lot of the outer terminal's special keys. `define-key' can
;;     be used to inform the application of the extended capabilities.
;;
;;  3. The terminal in question has extended features, that go well beyond the
;;     set of features that can be mapped into termcap/terminfo. For example,
;;     xterm has the formatOtherKeys option, that allows for a lot of extra key
;;     combinations to yield escape-sequences. For example, the terminal can be
;;     configured so that Ctrl-9 sends "ESC [ 5 7 ; 5 u", instead of ignoring
;;     the combination or just sending "9". `define-key' can be used to inform
;;     the application of such extensions.
;;
;; This is implemented like this:
;;
;; We pick a large number (see `user-key-boundary' below), that we use to
;; register key-names with ncurses. As you know, special keys like KEY_HOME are
;; mappend to integers. `define-key' uses the same facility. We just need to
;; make sure that we don't clash with already defined keys. So we set the
;; `user-key-boundary' to something reasonably large.
;;
;; Then internally, we store all key definitions in a hashmap that is indexed
;; by the user-key's intended name (the integer for ncurses' special key
;; facility) and stores the id, along with the intended character-sequence and
;; a record of whether or not the key was registered with ncurses yet.
;;
;; To actually register the keys with ncurses, the `update-user-keys' function
;; needs to be called. That is the only part of this module, that actually
;; interfaces (ncurses curses).

(define-module (scui user-key)
  #:use-module (ice-9 control)
  #:use-module (srfi srfi-9)
  #:use-module (ncurses curses)
  #:export (key-is-user-key?
            add-user-key
            get-user-key
            get-user-key-sequence
            get-user-key-id
            get-user-key-name
            update-user-keys))

(define-record-type <user-key>
  (make-user-key* id sequence registered?)
  user-key?
  (id user-key-id)
  (sequence user-key-sequence user-key-sequence-set!)
  (registered? user-key-registered? user-key-registered-set!))

(define (make-user-key id sequence)
  (make-user-key* id sequence #f))

(define user-key-boundary #x2000)
(define user-keys (make-hash-table))
(define last-user-id user-key-boundary)
(define unknown-key-tag "(unknown key)")

(define (key-is-user-key? key)
  "Check if the integer KEY points to a user-defined key. If it is not a
user-defined key it is a curses-special key like KEY_HOME."
  (>= key user-key-boundary))

(define (get-user-key-name id)
  "Returns the name of a user-key that was assigned the integer ID. If ID is
not an assigned integer, the function returns #f.

This function is a reverse lookup in the user-keys hash-map. It requires linear
time to perform the look-up."
  (call/ec (lambda (escape-with)
             (hash-map->list (lambda (name data)
                               (let ((this-id (user-key-id data)))
                                 (if (= this-id id)
                                     (escape-with name))))
                             user-keys)
             #f)))

(define (get-user-key name)
  (hash-ref user-keys name))

(define (get-user-key-id name)
  "Resolve a user-key's string representation provided as NAME to its assigned
integer id and return that number. If NAME is not a registered key name, the
function returns #f."
  (let ((data (hash-ref user-keys name)))
    (if data
        (user-key-id data)
        #f)))

(define (add-user-key name seq)
  "Adds a character sequence SEQ to the user-key database as a key named NAME.

If NAME was not added before, the function returns `added'.

If NAME was indeed added before, the function return `updated' if the provided
character sequence SEQ changed in any way.

Otherwise nothing is changed and #f is returned."
  (let ((old-entry (hash-ref user-keys name)))
    (cond ((not old-entry)
           (hash-set! user-keys name (make-user-key last-user-id seq))
           (set! last-user-id (1+ last-user-id))
           'added)
          ((string<> (user-key-sequence old-entry) seq)
           (user-key-sequence-set! old-entry seq)
           (user-key-registered-set! old-entry #f)
           'updated)
          (else #f))))

(define (update-user-keys)
  "Process all entries in the `user-keys' database and call `define-key' for
each and every entry, that does not have the `registered?' bit set."
  (hash-map->list (lambda (name data)
                    (unless (user-key-registered? data)
                      (define-key (user-key-sequence data) (user-key-id data))
                      (user-key-registered-set! data #t)))
                  user-keys))

(define (parse-key-sequence seq)
  "Sequences may be defined as strings as well as as lists of characters.
In strings all of guile's string interpolation magic applies, especially:

  - \\xHH -> insert byte corresponding to the hex value \"HH\"

Thus, \\x1b may be used to insert an ASCII escape character into a string.
As a convenience, we allow \"^[\" to appear at the *beginning* of
sequences, too, to let the sequence start with an escape character (because
that is how many programs, like \"cat -v\" and various shells, display
them).

With that in mind the following calls will yield equivalent results:

  (new-key #:name \"C-RET\" #:seq \"^[[13;5u\")
  (new-key #:name \"C-RET\" #:seq \"\\x1b[13;5u\")
  (new-key #:name \"C-RET\" #:seq '(#\\esc #\\[ #\\1 #\\3 #\\; #\\5 #\\u))

The `key-watch-mode' will display \"\\x1b[13;5u\" in either case."
  (cond ((list? seq)
         (apply string seq))
        (else (if (string-prefix? "^[" seq)
                  (apply string (cons #\esc (cddr (string->list seq))))
                  seq))))

(define (get-user-key-sequence name)
  (string->list (parse-key-sequence (user-key-sequence (hash-ref user-keys name)))))
