;; Copyright (c) 2014 scui workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in doc/LICENCE.

(define-module (scui windows watch-key)
  #:use-module (ice-9 format)
  #:use-module (ncurses curses)
  #:use-module (scui key)
  #:use-module (scui user-key)
  #:export (watch-key))

(define watch:quit-count 0)
(define watch:required-quits 3)

(define (print-3 win key meta end)
  (addstr win (format #f "  ~a~a~a~%" key meta end)))

(define (tell-meta meta)
  (if meta " [meta-key-combination]" ""))

(define (tell-times n-left)
  (format #f " (~d more time~a to quit)"
          n-left
          (if (> n-left 1) "s" "")))

(define (tell-key win key meta)
  (print-3 win key (tell-meta meta) ""))

(define (tell-key-quit win key meta quits-left)
  (print-3 win key (tell-meta meta) (tell-times quits-left)))

(define (watch-key win input)
  (let ((key (process-input input)))
    (when (and (key? key) (not (key-ignore? key)))
      (let ((quit (eq? (key-raw key) #\q))
            (pretty-key (pretty-print-key key))
            (meta (key-meta? key)))
        (cond (quit
               (set! watch:quit-count (1+ watch:quit-count))
               (tell-key-quit win pretty-key meta (- watch:required-quits
                                                     watch:quit-count)))
              (else
               (set! watch:quit-count 0)
               (tell-key win pretty-key meta)))
        (< watch:quit-count watch:required-quits)))))

(define (watch-key-loop win)
  (addstr win "  --- watch-key mode, press `q' three times to exit ---\n\n")
  (refresh win)
  (let next-char ((key (getch win)))
    (let ((keep-going (watch-key win key)))
      (cond ((not keep-going) #f)
            (else (next-char (getch win)))))))

(define (pretty-print-key key)
  (let ((numeric (key-code-point key))
        (raw (key-raw key))
        (type (key-type key)))
    (case type
      ((character)
       (format #f "key: \"~a\"  (code-point: U+~4,'0x  decimal: ~d)"
               (cond ((zero? numeric) "^@")
                     ((key-is-7bit? key) (keyname numeric))
                     (else raw))
               numeric numeric))
      ((special)
       (format #f "Special key: ~a  (hex: 0x~4,'0x  decimal: ~d)"
               (special-keyname numeric) numeric numeric))
      ((user)
       (let ((name (get-user-key-name numeric)))
         (format #f "User defined key: ~a  (hex: 0x~4,'0x  decimal: ~d  sequence: ~s)"
                 name numeric numeric
                 (get-user-key-sequence name))))
      (else (format #f "invalid keycode (~a)" key)))))

(define (special-keyname num)
  (let ((name (keyname num)))
    (cond ((not (string-prefix? "KEY_" name)) name)
          (else (substring name 4)))))

(define (make-watch-key-window terminal)
  (make-log-like-window terminal))
