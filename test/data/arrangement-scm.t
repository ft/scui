;; -*- scheme -*-

(use-modules (test tap)
             (ice-9 match)
             (scui data arrangement))

(primitive-load "test/test-tap-cfg.scm")

(define-tap-test (pass-if-invalid-hint? hint reason*)
  (and (eq? (dim:type hint) 'invalid)
       (eq? (assq-ref (dim:arg hint) 'reason) reason*)))

(with-fs-test-bundle
    (no-plan)

  ;; fold-hint/same: Empty argument

  (define-test "fold-hint/same: Empty list returns [minimum 1]"
    (pass-if-equal? (fold-hint/same '())
                    (make-dimension-hint 'minimum 1)))

  ;; fold-hint/same: Single argument

  (define-test "fold-hint/same: minimum -> minimum"
    (let ((hint (make-dimension-hint 'minimum 10)))
      (pass-if-equal? (fold-hint/same (list hint)) hint)))

  (define-test "fold-hint/same: exact -> exact"
    (let ((hint (make-dimension-hint 'exact 10)))
      (pass-if-equal? (fold-hint/same (list hint)) hint)))

  (define-test "fold-hint/same: ratio -> minimum"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'ratio 10/100)))
                    (make-dimension-hint 'minimum 1)))

  ;; fold-hint/same: Multiple, starting at minimum

  (define-test "fold-hint/same: minimum -> minimum -> minimum"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'minimum 10)
                                          (make-dimension-hint 'minimum 23)))
                    (make-dimension-hint 'minimum 23)))

  (define-test "fold-hint/same: minimum -> exact -> exact"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'minimum 10)
                                          (make-dimension-hint 'exact 23)))
                    (make-dimension-hint 'exact 23)))

  (define-test "fold-hint/same: minimum -> ratio -> minimum"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'minimum 23)
                                          (make-dimension-hint 'ratio 10/100
                                                               #:minimum 42)))
                    (make-dimension-hint 'minimum 42)))

  (define-test "fold-hint/same: minimum (too big) -> exact -> invalid"
    (pass-if-invalid-hint?
     (fold-hint/same (list (make-dimension-hint 'minimum 42)
                           (make-dimension-hint 'exact 23)))
     'cannot-fit-exact-into-minimum))

  ;; fold-hint/same: Multiple, starting at exact

  (define-test "fold-hint/same: exact -> minimum -> exact"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'exact 23)
                                          (make-dimension-hint 'minimum 10)))
                    (make-dimension-hint 'exact 23)))

  (define-test "fold-hint/same: exact -> exact -> exact"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'exact 23)
                                          (make-dimension-hint 'exact 23)))
                    (make-dimension-hint 'exact 23)))

  (define-test "fold-hint/same: exact -> ratio -> exact"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'exact 23)
                                          (make-dimension-hint 'ratio 10/100)))
                    (make-dimension-hint 'exact 23)))

  (define-test "fold-hint/same: exact -> minimum (too big) -> invalid"
    (pass-if-invalid-hint?
     (fold-hint/same (list (make-dimension-hint 'exact 23)
                           (make-dimension-hint 'minimum 42)))
     'cannot-fit-minimum-into-exact))

  (define-test "fold-hint/same: exact -> exact (other) -> invalid"
    (pass-if-invalid-hint?
     (fold-hint/same (list (make-dimension-hint 'exact 23)
                           (make-dimension-hint 'exact 42)))
     'exact-hint-mismatch))

  ;; fold-hint/same: Multiple, starting at ratio

  (define-test "fold-hint/same: ratio -> minimum -> minimum"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'ratio 10/100)
                                          (make-dimension-hint 'minimum 23)))
                    (make-dimension-hint 'minimum 23)))

  (define-test "fold-hint/same: ratio -> exact -> exact"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'ratio 10/100)
                                          (make-dimension-hint 'exact 23)))
                    (make-dimension-hint 'exact 23)))

  (define-test "fold-hint/same: ratio -> ratio -> minimum"
    (pass-if-equal? (fold-hint/same (list (make-dimension-hint 'ratio 10/100)
                                          (make-dimension-hint 'ratio 5/100)))
                    (make-dimension-hint 'minimum 1)))

  ;; fold-hint/sum: Empty argument

  (define-test "fold-hint/sum: Empty list returns [exact 0]"
    (pass-if-equal? (fold-hint/sum '())
                    (make-dimension-hint 'exact 0)))

  ;; fold-hint/sum: Single argument

  (define-test "fold-hint/sum: minimum -> minimum"
    (let ((hint (make-dimension-hint 'minimum 10)))
      (pass-if-equal? (fold-hint/sum (list hint)) hint)))

  (define-test "fold-hint/sum: exact -> exact"
    (let ((hint (make-dimension-hint 'exact 10)))
      (pass-if-equal? (fold-hint/sum (list hint)) hint)))

  (define-test "fold-hint/sum: ratio -> minimum"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'ratio 10/100
                                                              #:minimum 10)))
                    (make-dimension-hint 'minimum 10)))

  ;; fold-hint/sum: Multiple, starting at minimum

  (define-test "fold-hint/sum: minimum -> minimum -> minimum"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'minimum 10)
                                         (make-dimension-hint 'minimum 23)))
                    (make-dimension-hint 'minimum 33)))

  (define-test "fold-hint/sum: minimum -> exact -> minimum"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'minimum 10)
                                         (make-dimension-hint 'exact 23)))
                    (make-dimension-hint 'minimum 33)))

  (define-test "fold-hint/sum: minimum -> ratio -> minimum"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'minimum 23)
                                         (make-dimension-hint 'ratio 10/100
                                                              #:minimum 10)))
                    (make-dimension-hint 'minimum 33)))

  ;; fold-hint/sum: Multiple, starting at exact

  (define-test "fold-hint/sum: exact -> minimum -> minimum"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'exact 23)
                                         (make-dimension-hint 'minimum 10)))
                    (make-dimension-hint 'minimum 33)))

  (define-test "fold-hint/sum: exact -> exact -> exact"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'exact 23)
                                         (make-dimension-hint 'exact 23)))
                    (make-dimension-hint 'exact 46)))

  (define-test "fold-hint/sum: exact -> ratio -> minimum"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'exact 23)
                                         (make-dimension-hint 'ratio 10/100
                                                              #:minimum 10)))
                    (make-dimension-hint 'minimum 33)))

  ;; fold-hint/sum: Multiple, starting at ratio

  (define-test "fold-hint/sum: ratio -> minimum -> minimum"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'ratio 10/100
                                                              #:minimum 10)
                                         (make-dimension-hint 'minimum 23)))
                    (make-dimension-hint 'minimum 33)))

  (define-test "fold-hint/sum: ratio -> exact -> minimum"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'ratio 10/100
                                                              #:minimum 10)
                                         (make-dimension-hint 'exact 23)))
                    (make-dimension-hint 'minimum 33)))

  (define-test "fold-hint/sum: ratio -> ratio -> minimum"
    (pass-if-equal? (fold-hint/sum (list (make-dimension-hint 'ratio 10/100
                                                              #:minimum 10)
                                         (make-dimension-hint 'ratio 5/100
                                                              #:minimum 20)))
                    (make-dimension-hint 'minimum 30))))
