;; -*- scheme -*-

(use-modules (test tap)
             (ice-9 match)
             (scui data size-hint))

(primitive-load "test/test-tap-cfg.scm")

(force-import (scui data size-hint)
              make-size-hint*)

(define hint-data-tests
  '((#t (1 . 1) (1 . 1))
    (#t (* . 1) (1 . 1))
    (#t (1 . 1) (* . 1))
    (#t (* . 1) (* . 1))
    (#f (1 . *) (1 . 1))
    (#f (1 . 1) (1 . *))
    (#t (19.0 . 1) (19.0 . 1))
    (#t (19/100 . 1) (19/100 . 1))
    (#f (190/100 . 1) (19/100 . 1))
    (#f (1.0+1.0i . 1) (19/100 . 1))))

(define make-sources
  '((#t 1 1)
    (#f 1 190/100)
    (#f 1 19/100 1 *)
    (#f 1 1 * 1)
    (#f 1.0+1.0i 1 1 1)
    (#t 10.0 1 10 1)))

(define conversion-tests
  '((((1 . 1) (1 . 1)) ((1 . 1) (1 . 1)))
    (((1/10 . 1) (1 . 1)) ((* . 1) (1 . 1)))
    (((1 . 1) (1/10 . 1)) ((1 . 1) (* . 1)))
    (((1/10 . 1) (1/10 . 1)) ((* . 1) (* . 1)))
    (((10.0 . 1) (1 . 1)) ((* . 1) (1 . 1)))
    (((1 . 1) (10.0 . 1)) ((1 . 1) (* . 1)))
    (((10.0 . 1) (10.0 . 1)) ((* . 1) (* . 1)))
    ((1 . 1) (1 . 1))
    ((1/10 . 1) (* . 1))
    ((10.0 . 1) (* . 1))))

(define msh #f)

(define (source->hint src)
  (match src
    ((w h) (msh w h))
    ((w h mw mh) (msh w h #:min-width mw #:min-height mh))))

(with-fs-test-bundle
 (plan (+ (length hint-data-tests)
          (length make-sources)
          (length conversion-tests)
          (length (filter (lambda (x) (not (car x))) make-sources))
          2))
 (for-each (lambda (test)
             (let ((exp (car test))
                   (hint (cdr test)))
               (define-test (format #f "(size-hint? ~a) => ~a"
                                    hint (if exp "true" "false"))
                 (if exp
                     (pass-if-true (size-hint? hint))
                     (pass-if-false (size-hint? hint))))))
           hint-data-tests)
 (set! msh make-size-hint*)
 (for-each (lambda (test)
             (let ((exp (car test))
                   (hint (source->hint (cdr test))))
               (define-test (format #f "(size-hint? ~a) => ~a"
                                    hint (if exp "true" "false"))
                 (if exp
                     (pass-if-true (size-hint? hint))
                     (pass-if-false (size-hint? hint))))))
           make-sources)
 (set! msh make-size-hint)
 (for-each (lambda (test)
             (define-test (format #f "make-size-hint throws; args: ~a"
                                  (cdr test))
               (pass-if-exception 'scui/broken-hint-arguments
                                  (source->hint (cdr test)))))
           (filter (lambda (x) (not (car x))) make-sources))
 (for-each (lambda (test)
             (let ((in (car test))
                   (out (cadr test)))
               (define-test (format #f "(hint->demand ~a) => ~a" in out)
                 (pass-if-equal? (hint->demand in) out))))
           conversion-tests)
 (define-test "width-hint works"
   (pass-if-equal? (width-hint
                    (make-size-hint* 42 53 #:min-width 23 #:min-height 42))
                   '(42 . 23)))
 (define-test "height-hint works"
   (pass-if-equal? (height-hint
                    (make-size-hint* 42 53 #:min-width 23 #:min-height 42))
                   '(53 . 42))))
