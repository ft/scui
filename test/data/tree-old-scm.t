;; -*- scheme -*-

(use-modules (test tap)
             (scui data tree-old))

(primitive-load "test/test-tap-cfg.scm")

(force-import (scui data tree-old)
              make-node
              list-insert!
              node-is-in-ancestry?
              node-ancestry
              node-parent
              node-children
              node-payload-set!)

(define initial-tests (make-labeled-values node?
                                           node-is-root?
                                           node-is-leaf?
                                           tree?))

(with-fs-test-bundle
 (plan (+ (length initial-tests)
          131))

 (map (lambda (x)
        (define-test (format #f "create tree (~a)" (car x))
          (pass-if-true ((cdr x) (make-tree)))))
      initial-tests)

 (let ((n (make-node 'par
                     '(a b c)
                     '(some pay load))))
   (define-test "node? passes with normal node"
     (pass-if-true (node? n)))
   (define-test "node-parent"
     (pass-if-eq? (node-parent n)
                  'par))
   (define-test "node-children"
     (pass-if-equal? (node-children n)
                     '(a b c)))
   (define-test "node-payload"
     (pass-if-equal? (node-payload n)
                     '(some pay load))))

 (let ((t (make-tree)))
   (cons-children! t '(aaa bbb ccc))
   (define-test "node-ref with too large argument returns gracefully"
     (pass-if-false (node-ref t
                              ;; Indexing starts at 0, so length is too big.
                              (length (node-children t)))))
   (define-test "node-ref* with too large argument returns gracefully"
     (pass-if-false (node-ref* t
                               ;; Indexing starts at 0, so length is too big.
                               (length (node-children t)))))
   (define-test "node-ref* 1"
     (pass-if-eq? (node-ref* t 0)
                  'aaa))
   (define-test "node-ref* 2"
     (pass-if-eq? (node-ref* t 1)
                  'bbb))
   (cons-children! t '(000))
   (define-test "node-ref* 3"
     (pass-if-eq? (node-ref* t 0)
                  '000))
   (define-test "node-ref* 4"
     (pass-if-eq? (node-ref* t 3)
                  'ccc))
   (append-children! t '(yyy zzz))
   (define-test "node-ref* 5"
     (pass-if-eq? (node-ref* t 5)
                  'zzz))
   (define-test "node-ref"
     (pass-if-true (node? (node-ref t 3))))
   (define-test "parent is root"
     (pass-if-true (node-is-root?
                    (node-parent (node-ref t 4)))))
   (define-test "parent is original t, first child: '000"
     ;; Pick any child of "t", get its parent (which should be "t" again), get
     ;; its children and look at the payload of the first one of those. Should
     ;; be the last one we cons'ed, thus '000.
     (pass-if-eq? (node-payload
                   (car (node-children
                         (node-parent (node-ref t 4)))))
                  '000)))

 (let ((t (make-tree))
       (pre-lr '(this-is-root aa 11 more is less 22 33
                              bb 44 55 66
                              cc 77 88 less is more 99))
       (pre-rl '(this-is-root cc 99 88 more is less 77
                              bb 66 55 44
                              aa 33 22 11 less is more))
       (post-lr '(more is less 11 22 33 aa
                       44 55 66 bb
                       77 less is more 88 99 cc this-is-root))
       (post-rl '(99 more is less 88 77 cc
                     66 55 44 bb
                     33 22 less is more 11 aa this-is-root))
       (level-lr '(this-is-root aa bb cc 11 22 33 44 55 66 77 88 99
                                more is less less is more))
       (level-rl '(this-is-root cc bb aa 99 88 77 66 55 44 33 22 11
                                more is less less is more)))
   (node-payload-set! t 'this-is-root)
   (cons-children! t '(aa bb cc))
   (cons-children! (node-ref t 0) '(11 22 33))
   (cons-children! (node-ref t 1) '(44 55 66))
   (cons-children! (node-ref t 2) '(77 88 99))
   (cons-children! (node-ref (node-ref t 2) 1) '(less is more))
   (cons-children! (node-ref (node-ref t 0) 0) '(more is less))
   (define-test "flatten (defaults): pre-order, left-to-right"
     (pass-if-equal? (flatten t) pre-lr))
   (define-test "flatten: pre-order, right-to-left"
     (pass-if-equal? (flatten t #:left-to-right? #f #:order 'pre)
                     pre-rl))
   (define-test "flatten: post-order, left-to-right"
     (pass-if-equal? (flatten t #:order 'post)
                     post-lr))
   (define-test "flatten: post-order, right-to-left"
     (pass-if-equal? (flatten t #:left-to-right? #f #:order 'post)
                     post-rl))
   (define-test "flatten: level-order, left-to-right"
     (pass-if-equal? (flatten t #:order 'level)
                     level-lr))
   (define-test "flatten: level-order, right-to-left"
     (pass-if-equal? (flatten t #:left-to-right? #f #:order 'level)
                     level-rl))
   (define-test "node-left-sibling of node 0"
     (pass-if-false (node-left-sibling (node-ref t 0))))
   (map (lambda (n)
          (define-test (format #f "node-left-sibling of node ~d" n)
            (pass-if-eq? (node-left-sibling (node-ref t n))
                         (node-ref t (- n 1)))))
        '(1 2))
   (define-test "node-left-sibling with POST-PROC works"
     (pass-if-eq? (node-left-sibling (node-ref t 1) #:post-proc cadr)
                  (node-ref t 1)))
   (map (lambda (n)
          (define-test (format #f "node-right-sibling of node ~d" n)
            (pass-if-eq? (node-right-sibling (node-ref t n))
                         (node-ref t (+ n 1)))))
        '(0 1))
   (define-test "node-right-sibling of node 2"
     (pass-if-false (node-right-sibling (node-ref t 2))))
   (define-test "node-right-sibling with POST-PROC works"
     (pass-if-eq? (node-right-sibling (node-ref t 0) #:post-proc cadr)
                  (node-ref t 2)))
   (define-test "node-first-sibling works 1"
     (pass-if-eq? (node-ref t 0)
                  (node-first-sibling (node-ref t 1))))
   (define-test "node-first-sibling works 2"
     (pass-if-false (node-first-sibling (node-ref t 0))))
   (define-test "node-first-sibling with POST-PROC works"
     (pass-if-eq? (node-first-sibling (node-ref t 2) #:post-proc cadr)
                  (node-ref t 1)))
   (define-test "node-first-sibling with unset POST-PROC works"
     (pass-if-equal? (node-first-sibling (node-ref t 2) #:post-proc #f)
                     (node-children t)))
   (define-test "node-last-sibling works 1"
     (pass-if-eq? (node-ref t 2)
                  (node-last-sibling (node-ref t 1))))
   (define-test "node-last-sibling works 2"
     (pass-if-false (node-last-sibling (node-ref t 2))))
   (define-test "node-last-sibling with POST-PROC works"
     (pass-if-true (null? (node-last-sibling (node-ref t 0) #:post-proc cdr))))
   (let ((copy (tree-copy t)))
     (define-test "copy is not the same object as original"
       (pass-if-not-eq? t copy))
     (define-test "copied: flatten (defaults): pre-order, left-to-right"
       (pass-if-equal? (flatten copy) pre-lr))
     (define-test "copied: flatten: pre-order, right-to-left"
       (pass-if-equal? (flatten copy #:left-to-right? #f #:order 'pre)
                       pre-rl))
     (define-test "copied: flatten: post-order, left-to-right"
       (pass-if-equal? (flatten copy #:order 'post)
                       post-lr))
     (define-test "copied: flatten: post-order, right-to-left"
       (pass-if-equal? (flatten copy #:left-to-right? #f #:order 'post)
                       post-rl))
     (define-test "copied: flatten: level-order, left-to-right"
       (pass-if-equal? (flatten copy #:order 'level)
                       level-lr))
     (define-test "copied: flatten: level-order, right-to-left"
       (pass-if-equal? (flatten copy #:left-to-right? #f #:order 'level)
                       level-rl))))

 (let* ((t (make-tree #:payload 'start
                      #:children
                      (list (make-tree #:payload 'next-l0
                                       #:children
                                       (list (make-tree #:payload 'next-ll1)))
                            (make-tree #:payload 'next-r0
                                       #:children
                                       (list (make-tree #:payload 'next-rl1)
                                             (make-tree #:payload 'next-rr1))))))
        (rel '((next-l0 . start)
               (next-r0 . start)
               (next-ll1 . next-l0)
               (next-rl1 . next-r0)
               (next-rr1 . next-r0)))
        (sub-node (node-ref t 1)))
   (define-test "nested make-tree flattens properly"
     (pass-if-equal? (flatten t)
                     '(start next-l0 next-ll1 next-r0 next-rl1 next-rr1)))
   (define-test "nested make-tree produces intact tree"
     (pass-if-true
      (traverse-fold (lambda (item acc)
                       (or (node-is-root? item)
                           (and acc
                                (eq? (node-payload (node-parent item))
                                     (cdr (assq (node-payload item) rel))))))
                     #t t #:payload-only? #f)))
   (let ((subt (node-remove! sub-node)))
     (define-test "node-remove! works 1"
       (pass-if-equal? (flatten t) '(start next-l0 next-ll1)))
     (define-test "node-remove! works 2"
       (pass-if-equal? (flatten subt) '(next-r0 next-rl1 next-rr1)))
     (define-test "node-remove! works 3"
       (pass-if-true (node-is-root? subt)))
     (let ((copy (tree-copy sub-node)))
       (define-test "tree-copy works"
         (pass-if-equal? (flatten copy) '(next-r0 next-rl1 next-rr1)))
       (node-cons! sub-node t)
       (define-test "node-cons! works"
         (pass-if-equal? (flatten t)
                         '(start next-r0 next-rl1 next-rr1 next-l0 next-ll1)))
       (define-test "node-cons! makes non-root leaf out of sub-node"
         (pass-if-false (node-is-root? sub-node)))
       (define-test "node-cons! sets parent of sub-node to original tree"
         (pass-if-eq? (node-parent sub-node) t))
       (node-append! copy t)
       (define-test "node-append! works"
         (pass-if-equal? (flatten t)
                         '(start next-r0 next-rl1 next-rr1 next-l0 next-ll1
                                 next-r0 next-rl1 next-rr1))))))
 (let* ((t (make-tree
            #:payload 'start
            #:children
            (list
             (make-tree #:payload 'aaa
                        #:children (list (make-tree #:payload 'ccc)
                                         (make-tree #:payload 'ddd)
                                         (make-tree #:payload 'eee)))
             (make-tree #:payload 'bbb
                        #:children
                        (list (make-tree #:payload 'ggg)
                              (make-tree #:payload 'hhh))))))
        (node-a (node-ref t 0))
        (node-b (node-ref t 1))
        (node-c (node-ref node-a 0))
        (node-d (node-ref node-a 1))
        (node-e (node-ref node-a 2)))
   (define-test "small flatten-test, level-order"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start aaa bbb ccc ddd eee ggg hhh)))
   (node-swap! node-a node-b)
   (define-test "node-swap! works"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start bbb aaa ggg hhh ccc ddd eee)))
   (node-swap! node-a node-b)
   (define-test "re-swap to get original tree back"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start aaa bbb ccc ddd eee ggg hhh)))
   (node-swap! node-a node-b #:with-subtrees #f)
   (define-test "node-swap! without subtrees works"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start bbb aaa ccc ddd eee ggg hhh)))
   (node-swap! node-a node-b #:with-subtrees #f)
   (define-test "node-swap! without subtrees back for original structure"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start aaa bbb ccc ddd eee ggg hhh)))
   (node-swap! node-a node-c #:with-subtrees #f)
   (define-test "payload-swap with parent and child flattens correctly"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start ccc bbb aaa ddd eee ggg hhh)))
   (define-test "    ...reparents new parent correctly"
     (pass-if-eq? (node-parent node-c) t))
   (define-test "    ...reparents new child correctly"
     (pass-if-eq? (node-parent node-a) node-c))
   (node-swap! node-a node-c #:with-subtrees #f)
   (define-test "node-swap! without subtrees back for original structure"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start aaa bbb ccc ddd eee ggg hhh)))
   (node-swap! node-b t #:with-subtrees #f)
   (define-test "node-swap! without subtrees works with root-nodes"
     (pass-if-equal? (flatten node-b #:order 'level)
                     '(bbb aaa start ccc ddd eee ggg hhh)))
   (define-test "node-b is root now"
     (pass-if-true (node-is-root? node-b)))
   (define-test "node-b is parent of t"
     (pass-if-eq? (node-parent t) node-b))
   (node-swap! node-b t #:with-subtrees #f)
   (define-test "node-swap! without subtrees back for original structure"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start aaa bbb ccc ddd eee ggg hhh)))
   (define-test "node-swap! does nothing if nodes are the same"
     (pass-if-eq? (node-swap! node-a node-a) 'same-nodes))
   (define-test "node-swap! with-subtrees doesn't create graphs 1"
     (pass-if-false (node-swap! t node-a)))
   (define-test "node-swap! with-subtrees doesn't create graphs 2"
     (pass-if-false (node-swap! t node-c)))
   (define-test "node-swap! with-subtrees doesn't create graphs 3"
     (pass-if-false (node-swap! node-a node-c)))
   (define-test "node-to-beginning!"
     (pass-if-true (node-to-beginning! node-d)))
   (define-test "node-d moved to front"
     (pass-if-false (node-left-sibling node-d)))
   (define-test "node-c now right sibling of node-d"
     (pass-if-eq? (node-right-sibling node-d)
                  node-c))
   (define-test "node-d can't be moved to front two times in a row"
     (pass-if-false (node-to-beginning! node-d)))
   (define-test "node-c can be moved to front again"
     (pass-if-true (node-to-beginning! node-c)))
   (define-test "now node-d is right sibling of node-c again"
     (pass-if-eq? (node-right-sibling node-c)
                  node-d))
   (define-test "node-to-end!"
     (pass-if-true (node-to-end! node-d)))
   (define-test "node-d moved to end"
     (pass-if-false (node-right-sibling node-d)))
   (define-test "node-e now left sibling of node-d"
     (pass-if-eq? (node-left-sibling node-d)
                  node-e))
   (define-test "node-d can't be moved to the end two times in a row"
     (pass-if-false (node-to-end! node-d)))
   (define-test "node-e can be moved to the end again"
     (pass-if-true (node-to-end! node-e)))
   (define-test "now node-e is right sibling of node-e again"
     (pass-if-eq? (node-right-sibling node-d)
                  node-e))
   (define-test "back to original structure"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start aaa bbb ccc ddd eee ggg hhh)))
   (define-test "rotate-to-right! moves node"
     (pass-if-true (node-rotate-right! node-d)))
   (define-test "rotate-to-right! moves node correctly"
     (pass-if-equal? (flatten node-a)
                     '(aaa ccc eee ddd)))
   (define-test "rotate-to-right! moves last node"
     (pass-if-true (node-rotate-right! node-d)))
   (define-test "rotate-to-right! rolls end to front"
     (pass-if-equal? (flatten node-a)
                     '(aaa ddd ccc eee)))
   (define-test "rotate-to-right! back to original place"
     (pass-if-true (node-rotate-right! node-d)))
   (define-test "back to original structure"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start aaa bbb ccc ddd eee ggg hhh)))
   (define-test "rotate-to-right! moves node"
     (pass-if-true (node-rotate-left! node-d)))
   (define-test "rotate-to-left! moves node correctly"
     (pass-if-equal? (flatten node-a)
                     '(aaa ddd ccc eee)))
   (define-test "rotate-to-left! moves last node"
     (pass-if-true (node-rotate-left! node-d)))
   (define-test "rotate-to-left! rolls end to front"
     (pass-if-equal? (flatten node-a)
                     '(aaa ccc eee ddd)))
   (define-test "rotate-to-left! back to original place"
     (pass-if-true (node-rotate-left! node-d)))
   (define-test "back to original structure"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start aaa bbb ccc ddd eee ggg hhh)))
   (define-test "node-insert! inserts"
     (pass-if-true (node-insert! (make-tree #:payload 'new-one-0) node-a 2)))
   (define-test "node-insert! inserted at the right place"
     (pass-if-equal? (flatten node-a)
                     '(aaa ccc ddd new-one-0 eee)))
   (define-test "node-insert! inserts at the beginning"
     (pass-if-true (node-insert! (make-tree #:payload 'new-one-1) node-a 0)))
   (define-test "node-insert! inserted at the beginning"
     (pass-if-equal? (flatten node-a)
                     '(aaa new-one-1 ccc ddd new-one-0 eee)))
   (define-test "node-insert! inserts at the end manually"
     (pass-if-true (node-insert! (make-tree #:payload 'new-one-2)
                                 node-a
                                 (length (node-children node-a)))))
   (define-test "node-insert! inserted at the end"
     (pass-if-equal? (flatten node-a)
                     '(aaa new-one-1 ccc ddd new-one-0 eee new-one-2)))
   (define-test "node-insert! inserts at the end with negative index"
     (pass-if-true (node-insert! (make-tree #:payload 'new-one-3) node-a -1)))
   (define-test "node-insert! inserted at the end"
     (pass-if-equal? (flatten node-a)
                     '(aaa new-one-1 ccc ddd new-one-0 eee new-one-2 new-one-3)))
   (define-test "node-insert! inserts at the beginning with negative index"
     (pass-if-true (node-insert! (make-tree #:payload 'new-one-4)
                                 node-a
                                 (* -1 (+ 1 (length (node-children node-a)))))))
   (define-test "node-insert! inserted at the beginning"
     (pass-if-equal? (flatten node-a)
                     '(aaa new-one-4 new-one-1 ccc ddd new-one-0
                           eee new-one-2 new-one-3)))
   (define-test "node-insert! inserts somewhere with negative index"
     (pass-if-true (node-insert! (make-tree #:payload 'new-one-5) node-a -3)))
   (define-test "node-insert! inserted at the beginning"
     (pass-if-equal? (flatten node-a)
                     '(aaa new-one-4 new-one-1 ccc ddd new-one-0
                           eee new-one-5 new-one-2 new-one-3)))
   (let ((node-a (make-tree #:payload 'aaa))
         (node-b (make-tree #:payload 'bbb)))
     (node-insert! node-a node-b 0)
     (define-test "node-insert! into leaf-node works"
       (pass-if-equal? (flatten node-b)
                       '(bbb aaa))))
   (define-test "node-move! refuses to move root-node"
     (pass-if-false (node-move! t node-a 1)))
   (define-test "node-move! refuses to move ancestor into subtree."
     (pass-if-false (node-move! node-a node-e 0)))
   (define-test "node-move! respects index limits"
     (pass-if-false (node-move! node-a node-b 3)))
   (define-test "node-move! respects negative index limits"
     (pass-if-false (node-move! node-a node-b -4)))
   (define-test "node-move! moves leaf-node"
     (pass-if-true (node-move! (node-ref node-a 4) ;; 'new-one-0
                               node-b 1)))
   (define-test "node-move! moves to right place"
     (pass-if-equal? (flatten node-b)
                     '(bbb ggg new-one-0 hhh)))
   (define-test "node-move! moves non-ancestor non-leaf node"
     (pass-if-true (node-move! node-b node-a 4)))
   (define-test "node-move! moves non-ancestor non-leaf node correctly"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start aaa new-one-4 new-one-1 ccc ddd bbb eee
                             new-one-5 new-one-2 new-one-3 ggg new-one-0 hhh)))
   (define-test "node-move! moves node-b back"
     (pass-if-true (node-move! (node-ref node-a 4) t 0)))
   (define-test "node-move! moves node-b back correctly"
     (pass-if-equal? (flatten t #:order 'level)
                     '(start bbb aaa ggg new-one-0 hhh new-one-4 new-one-1
                             ccc ddd eee new-one-5 new-one-2 new-one-3))))
 (let* ((t (make-tree
            #:payload 'this-is-the-beginning
            #:children
            (list
             (make-tree
              #:payload 'aaa
              #:children
              (list (make-tree
                     #:payload 'ddd
                     #:children
                     (list (make-tree #:payload 'kkk)
                           (make-tree #:payload 'lll)))
                    (make-tree
                     #:payload 'eee
                     #:children
                     (list (make-tree #:payload 'ggg)
                           (make-tree #:payload 'hhh)
                           (make-tree #:payload 'iii)
                           (make-tree #:payload 'jjj)))
                    (make-tree #:payload 'fff)))
             (make-tree #:payload 'bbb)
             (make-tree
              #:payload 'ccc
              #:children
              (list (make-tree
                     #:payload 'ddd
                     #:children
                     (list (make-tree #:payload 'mmm)
                           (make-tree #:payload 'nnn)))
                    (make-tree
                     #:payload 'ttt
                     #:children
                     (list (make-tree #:payload 'ooo)
                           (make-tree #:payload 'ppp)
                           (make-tree #:payload 'qqq)
                           (make-tree #:payload 'rrr)))
                    (make-tree #:payload 'sss))))))
        (flattened-t '(this-is-the-beginning aaa ddd kkk lll eee ggg
                                             hhh iii jjj fff
                                             bbb
                                             ccc ddd mmm nnn ttt ooo
                                             ppp qqq rrr sss))
        (deep-node (node-ref (node-ref (node-ref t 2) 1) 2)))
   (define-test "another default-flatten test"
     (pass-if-equal? (flatten t)
                     flattened-t))
   (define-test "find a deep node"
     (pass-if-equal? (node-payload deep-node)
                     'qqq))
   (define-test "deep-node is leaf"
     (pass-if-true (node-is-leaf? deep-node)))
   (define-test "deep-node's ancestry looks right"
     (pass-if-equal? (map (lambda (x) (node-payload x))
                          (node-ancestry deep-node))
                     '(this-is-the-beginning ccc ttt)))
   (define-test "root is ancestor of any node, including deep-node"
     (pass-if-true (node-is-in-ancestry? t deep-node)))
   (define-test "node of other subtree isn't ancestor"
     (pass-if-false (node-is-in-ancestry? (node-ref t 0) deep-node)))
   (define-test "non-root ancestor is recognised as ancestor"
     (pass-if-true (node-is-in-ancestry? (node-ref t 2) deep-node)))
   (define-test "traverse-filter works"
     (pass-if-equal? (traverse-filter (lambda (x)
                                        (string> (symbol->string x)
                                                 "now-what?"))
                                      t
                                      #:order 'level)
                     '(this-is-the-beginning ttt sss ooo ppp qqq rrr)))
   (define-test "traverse-search works"
     (pass-if-equal? (traverse-search (lambda (x)
                                        (let ((s (symbol->string x)))
                                          (and (<= (string-length s) 3)
                                               (string> s "now-what?"))))
                                      t
                                      #:order 'level)
                     '(ttt)))
   (define-test "traverse-search returns the empty list if proc never succeeds"
     (pass-if-eq? (traverse-search (lambda (x) #f) t #:order 'level)
                  '()))
   (let ((mutate (lambda (x)
                   (string-concatenate
                    (list "batman! " (symbol->string x))))))
     (define-test "traverse-map works"
       (pass-if-equal? (traverse-map mutate t)
                       (map mutate flattened-t)))))
 (let ((lst (list 'aa 'bb 'cc 'dd 'ee 'ff)))
   (list-insert! lst 3 'FOO)
   (define-test "list-insert! in the middle works"
     (pass-if-equal? lst
                     '(aa bb cc FOO dd ee ff)))
   (list-insert! lst 0 'BAR)
   (define-test "list-insert! at the beginning works"
     (pass-if-equal? lst
                     '(BAR aa bb cc FOO dd ee ff)))
   (list-insert! lst (length lst) 'BAZ)
   (define-test "list-insert! at the end works"
     (pass-if-equal? lst
                     '(BAR aa bb cc FOO dd ee ff BAZ)))))
