;; -*- scheme -*-

(use-modules (ice-9 pretty-print) (test tap)
             (scui data tree)
             (scui data zipper))

(primitive-load "test/test-tap-cfg.scm")

(force-import (scui data tree)
              tree-select-n
              tree-zip)

;; If #t, some tests use pp (defined below) to output some data structures.
(define *debug* #f)

(define (pp obj)
  (pretty-print obj #:per-line-prefix "# "))

(define (tree-compare-sharing t1 t2)
  (define (step z)
    ((z:cont z) #f))
  (define (obj->nlist obj)
    (if (node? obj)
        (tree->nlist obj)
        obj))
  (define (return t1n t2n sn)
    (list (cons 't1-nodes (map obj->nlist (reverse t1n)))
          (cons 't2-nodes (map obj->nlist (reverse t2n)))
          (cons 'shared-nodes (map tree->nlist (reverse sn)))))
  (let loop ((cursor1 (tree-zip t1)) (cursor2 (tree-zip t2))
             (t1-nodes '()) (t2-nodes '()) (shared-nodes '()))
    (cond ((or (not (zipper? cursor1)) (not (zipper? cursor2)))
           (return t1-nodes t2-nodes shared-nodes))
          ((not (zipper? cursor1))
           (return (cons 't1-finished-early t1-nodes) t2-nodes shared-nodes))
          ((not (zipper? cursor1))
           (return t1-nodes (cons 't2-finished-early t2-nodes) shared-nodes))
          (else
           (let ((n1 (z:node cursor1)) (n2 (z:node cursor2)))
             (if (eq? n1 n2)
                 (loop (step cursor1) (step cursor2)
                       t1-nodes t2-nodes (cons n1 shared-nodes))
                 (loop (step cursor1) (step cursor2)
                       (cons n1 t1-nodes) (cons n2 t2-nodes) shared-nodes)))))))

(with-fs-test-bundle
    (plan 22)

  (let ((obj (leaf-node 23)))
    (define-test "obj is leaf-node"
      (pass-if-true (leaf-node? obj)))
    (define-test "obj's payload is 23"
      (pass-if-= 23 (n:payload obj))))

  (let ((obj (make-node 'root (list (leaf-node 23)
                                    (leaf-node 42)
                                    (leaf-node 123)))))
    (define-test "obj is NOT a leaf-node"
      (pass-if-false (leaf-node? obj)))
    (define-test "obj has three sub-nodes"
      (pass-if-= 3 (length (n:nodes obj))))
    (define-test "obj's sub-nodes' payloads are: (23 42 123)"
      (pass-if-equal? '(23 42 123)
                      (map n:payload (n:nodes obj))))
    (define-test "obj's sub-nodes' are leaf-nodes"
      (pass-if-equal? '(#t #t #t)
                      (map leaf-node? (n:nodes obj)))))

  (define-test "tree-map on leaf-node returns leaf-node"
    (pass-if-true (leaf-node? (tree-map identity (leaf-node 23)))))

  (define-test "tree-map on leaf-node returns correct payload"
    (pass-if-= (n:payload (tree-map (lambda (x) (- (* 2 x) 4))
                                    (leaf-node 23)))
               42))

  (let ((obj (make-node 5 (list (leaf-node 23)
                                (leaf-node 42)
                                (leaf-node 123))))
        (x2 (lambda (x) (* 2 x))))
    (define-test "tree-map on shallow tree works"
      (pass-if-equal? (tree->nlist (tree-map x2 obj))
                      '(10 (46 84 246)))))

  (let ((obj (make-node 5 (list
                           (leaf-node 23)
                           (make-node 42 (list
                                          (leaf-node 11)
                                          (make-node 22 (list (leaf-node 31/2)
                                                              (leaf-node 31)))
                                          (leaf-node 50)
                                          (leaf-node 100)))
                           (leaf-node 123))))
        (x2 (lambda (x) (* 2 x))))
    (define-test "tree-map on deeper tree works"
      (pass-if-equal? (tree->nlist (tree-map x2 obj))
                      '(10 (46
                            (84 (22
                                 (44 (31
                                      62))
                                 100
                                 200))
                            246))))
    (define-test "Frontend for expressing trees works"
      ;; (! x) creates leaf-nodes with payload x.
      ;; (~ x (...)) creates a tree with nodes (...) and payload x.
      ;;
      ;; Any other expressions are inserted verbatim with a guard expression
      ;; that throws not-a-node if indeed the evaluated datum is not a node.
      ;;
      ;; An example for the latter is the (leaf-node 50) application in here.
      (pass-if-equal? obj (make-tree 5 ((! 23)
                                        (~ 42 ((! 11)
                                               (~ 22 ((! 31/2)
                                                      (! 31)))
                                               (leaf-node 50)
                                               (! 100)))
                                        (! 123))))))

  (let ((obj (make-tree 0 ((~ 1 ((! 4)
                                 (! 5)
                                 (! 6)))
                           (~ 2 ((! 7)
                                 (~ 8  ((! 13)
                                        (! 14)
                                        (! 15)))
                                 (! 9)))
                           (~ 3 ((! 10)
                                 (! 11)
                                 (! 12))))))
        (app (lambda (new acc)
               ;;(format #t "new: ~a to ~a~%" (n:payload new) acc)
               (append acc (list (n:payload new))))))
    (define-test "tree-fold: Level-Order traversal works (left-to-right)"
      (pass-if-equal? (tree-fold app '() obj #:order 'level)
                      '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15)))
    (define-test "tree-fold: Pre-Order traversal works (left-to-right)"
      (pass-if-equal? (tree-fold app '() obj #:order 'pre)
                      '(0 1 4 5 6 2 7 8 13 14 15 9 3 10 11 12)))
    (define-test "tree-fold: Post-Order traversal works (left-to-right)"
      (pass-if-equal? (tree-fold app '() obj #:order 'post)
                      '(4 5 6 1 7 13 14 15 8 9 2 10 11 12 3 0)))
    (define-test "tree-fold: Level-Order traversal works (right-to-left)"
      (pass-if-equal? (tree-fold app '() obj #:order 'level #:left-to-right? #f)
                      '(0 3 2 1 12 11 10 9 8 7 6 5 4 15 14 13)))
    (define-test "tree-fold: Pre-Order traversal works (right-to-left)"
      (pass-if-equal? (tree-fold app '() obj #:order 'pre #:left-to-right? #f)
                      '(0 3 12 11 10 2 9 8 15 14 13 7 1 6 5 4)))
    (define-test "tree-fold: Post-Order traversal works (right-to-left)"
      (pass-if-equal? (tree-fold app '() obj #:order 'post #:left-to-right? #f)
                      '(12 11 10 3 9 15 14 13 8 7 2 6 5 4 1 0))))

  (let* ((tree-1 (make-tree 0 ((~ 1 ((! 4)
                                     (! 5)
                                     (! 6)))
                               (~ 2 ((! 7)
                                     (~ 8  ((! 13)
                                            (! 14)
                                            (! 15)))
                                     (! 9)))
                               (~ 3 ((! 10)
                                     (! 11)
                                     (! 12))))))
         (tree-2 (make-tree 'a ((~ 10 ((! 40)
                                       (! 50)
                                       (! 60)))
                                (~ 20 ((! 70)
                                       (! 'abc)
                                       (! 90)))
                                (~ 30 ((! 100)
                                       (! 110)
                                       (! 120))))))
         ;; Copy tree-2
         (tree-2/copy (tree-copy tree-2))
         ;; Pick something from tree-2…
         (desired-node (tree-select-n 6 tree-2))
         ;; …and replace it.
         (tree-2/changed (zip-up ((z:cont desired-node)
                                  (leaf-node 'new-thing-in-here)))))
    ;; If the library works correctly, the copy of tree-2 and the original
    ;; should still carry the same content.
    (define-test "Result of tree-copy is equal? to the input"
      (pass-if-equal? tree-2 tree-2/copy))
    ;; But none of the structural elements (the tree's nodes) are the same.
    ;; That's what a copy should do. Note that this library's tree-copy utility
    ;; only creates copies of the structural elements, but NOT of the payload!
    (let ((diff (tree-compare-sharing tree-2 tree-2/copy)))
      (when *debug* (pp diff))
      (define-test "Result of tree-copy is does NOT share any structure"
        (pass-if-null? (assq-ref diff 'shared-nodes))))
    ;; The changed structure should NOT contain the same data, of course
    (define-test "Result of amending a tree is NOT equal? to the input"
      (pass-if-not-equal? tree-2 tree-2/changed))
    ;; But the original data structure and the changed variant should share
    ;; large portions!
    (let ((diff (tree-compare-sharing tree-2 tree-2/changed)))
      (when *debug* (pp diff))
      (define-test "Result of amending a tree DOES share lots of structure"
        (pass-if-equal? diff
                        '((t1-nodes     ; Structure that only appears in tree-2
                           (a ((10 (40 50 60))
                               (20 (70 abc 90))
                               (30 (100 110 120))))
                           (20 (70 abc 90))
                           70)
                          (t2-nodes     ; Structure that only appears in
                                        ; tree-2/changed
                           (a ((10 (40 50 60))
                               (20 (new-thing-in-here abc 90))
                               (30 (100 110 120))))
                           (20 (new-thing-in-here abc 90))
                           new-thing-in-here)
                          (shared-nodes ; Structure that shares between both.
                           (10 (40 50 60))
                           40 50 60 abc 90
                           (30 (100 110 120))
                           100 110 120)))))
    (let ((t (make-tree 'a ((~ 'b ((! 'e)
                                   (! 'f)
                                   (! 'g)))
                            (~ 'c ((! 'h)
                                   (~ 'i  ((! 'n)
                                           (! 'o)
                                           (! 'p)))
                                   (! 'j)))
                            (~ 'd ((! 'k)
                                   (! 'l)
                                   (! 'm)))))))
      (define-test "Merging two trees works"
        (pass-if-equal?
         (tree->nlist
          (tree-merge (lambda (state t1 t2)
                        (cons (symbol-append 'p: (car state) '< t1 '| t2 '>)
                              (+ 1 (cdr state))))
                      (lambda (state t1 t2)
                        (cons (symbol-append (car state) '< t1 ': t2 '>)
                              (+ 1 (cdr state))))
                      '(start . 0)
                      (tree-map (lambda (x) (symbol-append 'o x)) t)
                      (tree-map (lambda (x) (symbol-append 'x x)) t)))
         '((p:start<oa|xa> . 1)
           (((p:start<ob|xb> . 1)
             ((p:start<oe|xe> . 1)
              (p:start<oe:xe><of|xf> . 2)
              (p:start<oe:xe><of:xf><og|xg> . 3)))
            ((p:start<ob:xb><oc|xc> . 2)
             ((p:start<ob:xb><oh|xh> . 2)
              ((p:start<ob:xb><oh:xh><oi|xi> . 3)
               ((p:start<ob:xb><oh:xh><on|xn> . 3)
                (p:start<ob:xb><oh:xh><on:xn><oo|xo> . 4)
                (p:start<ob:xb><oh:xh><on:xn><oo:xo><op|xp> . 5)))
              (p:start<ob:xb><oh:xh><oi:xi><oj|xj> . 4)))
            ((p:start<ob:xb><oc:xc><od|xd> . 3)
             ((p:start<ob:xb><oc:xc><ok|xk> . 3)
              (p:start<ob:xb><oc:xc><ok:xk><ol|xl> . 4)
              (p:start<ob:xb><oc:xc><ok:xk><ol:xl><om|xm> . 5))))))))))
