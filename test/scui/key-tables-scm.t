;; -*- scheme -*-

(use-modules (test tap)
             (ncurses curses)
             (scui key)
             (scui user-key)
             (scui key-table))

(primitive-load "test/test-tap-cfg.scm")

(force-import (scui key-table)
              key-table?)

;; Define some user-keys.
(define some-user-keys '(("C-RET" . "^[[13;5u")
                         ("C-TAB" . "^[[9;5u")))
(map (lambda (x)
       (add-user-key (car x) (cdr x)))
     some-user-keys)
(update-user-keys)

(setlocale LC_ALL "")

;; Now add some key tables.
(define kt-a (add-table 'default #f #f))
(define kt-b (add-table 'mixer #f (lambda (x) +)))
(define kt-c (add-table 'default #t #f))
(define kt-d (add-table 'mixer #t (lambda (x) -)))

(define tables (make-labeled-values kt-a kt-b kt-c kt-d))

(with-fs-test-bundle
 (plan (+ (length tables)
          8))
 (map (lambda (x)
        (define-test (format #f "(key-table? ~a) => #t" (car x))
          (pass-if-true (key-table? (cdr x)))))
      tables)
 (define-test "(get-table 'default #f) => kt-a"
   (pass-if-eq? (get-table 'default #f)
                kt-a))
 (define-test "(get-table 'default #t) is NOT kt-a"
   (pass-if-not-eq? (get-table 'default #t)
                    kt-a))
 (define-test "(get-table 'mixer #t) => kt-d"
   (pass-if-eq? (get-table 'mixer #t)
                kt-d))
 (define-test "(get-table 'mixer #f) is NOT kt-d"
   (pass-if-not-eq? (get-table 'mixer #f)
                    kt-d))
 (bind-code (char->integer #\a) #f kt-a car)
 (define-test "lookup \"a\" works"
   (pass-if-eq? (lookup-key (process-input #\a)
                            kt-c
                            kt-a)
                car))
 (define-test "lookup \"a\" works; kt-b's catch-all triggers"
   (pass-if-eq? (lookup-key (process-input #\a)
                            kt-b
                            kt-a)
                +))
 (define-test "lookup unbound C-RET fails (with kt-a and kt-c: no catch-alls)"
   (pass-if-false (lookup-key (process-input (get-user-key-id "C-RET"))
                              kt-c kt-a)))
 (bind-code (get-user-key-id "C-RET") #t kt-c map)
 (define-test "lookup C-RET works after binding it"
   (pass-if-eq? (lookup-key (process-input (get-user-key-id "C-RET"))
                            kt-c kt-a)
                map)))
