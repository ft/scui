;; -*- scheme -*-

(use-modules (test tap)
             (ncurses curses)
             (scui key)
             (scui user-key))

(primitive-load "test/test-tap-cfg.scm")

;; Define some user-keys.
(define some-user-keys '(("C-RET" . "^[[13;5u")
                         ("C-TAB" . "^[[9;5u")
                         ("C-LEFT" . "^[[1;5D")
                         ("C-RIGHT" . "^[[1;5C")
                         ("C-UP" . "^[[1;5A")
                         ("C-DOWN" . "^[[1;5B")))
(map (lambda (x)
       (add-user-key (car x) (cdr x)))
     some-user-keys)
(update-user-keys)

(setlocale LC_ALL "")

(define some-printable-keys
  `((#\nul . "^@")
    (#\soh . "^A")
    (#\stx . "^B")
    (#\etx . "^C")
    (#\eot . "^D")
    (#\enq . "^E")
    (#\ack . "^F")
    (#\bel . "^G")
    (#\bs . "^H")
    (#\ht . "^I")
    (#\lf . "^J")
    (#\vt . "^K")
    (#\ff . "^L")
    (#\cr . "^M")
    (#\so . "^N")
    (#\si . "^O")
    (#\dle . "^P")
    (#\dc1 . "^Q")
    (#\dc2 . "^R")
    (#\dc3 . "^S")
    (#\dc4 . "^T")
    (#\nak . "^U")
    (#\syn . "^V")
    (#\etb . "^W")
    (#\can . "^X")
    (#\em . "^Y")
    (#\sub . "^Z")
    (#\esc . "^[")
    (#\fs . "^\\")
    (#\gs . "^]")
    (#\rs . "^^")
    (#\us . "^_")
    (#\space . " ")
    (#\a . "a")
    (,(integer->char #xe4) . ,(string (integer->char #xe4)))
    (,KEY_HOME . "HOME")
    (,KEY_PREVIOUS . "PREVIOUS")
    (,KEY_NEXT . "NEXT")
    (,KEY_PPAGE . "PPAGE")
    (,KEY_NPAGE . "NPAGE")
    (,KEY_UP . "UP")
    (,KEY_DOWN . "DOWN")
    (,KEY_LEFT . "LEFT")
    (,KEY_RIGHT . "RIGHT")
    (,(get-user-key-id "C-RET") . "C-RET")
    (,(get-user-key-id "C-TAB") . "C-TAB")
    (,(get-user-key-id "C-LEFT") . "C-LEFT")
    (,(get-user-key-id "C-RIGHT") . "C-RIGHT")
    (,(get-user-key-id "C-UP") . "C-UP")
    (,(get-user-key-id "C-DOWN") . "C-DOWN")))

(define some-type-tests
  `((,#\a . character)
    (,#\b . character)
    (,#\c . character)
    (,KEY_NEXT . special)
    (,KEY_DOWN . special)
    (,(get-user-key-id "C-RET") . user)
    (,(get-user-key-id "C-LEFT") . user)
    (-1 . invalid)))

(with-fs-test-bundle
 (plan (+ (length some-printable-keys)
          (length some-type-tests)
          15))
 (map (lambda (x)
        (let ((input (car x))
              (string (cdr x)))
          (define-test (format #f "process->printable: ~s" string)
            (pass-if-string=? (printable-key (process-input input))
                              string))))
      some-printable-keys)
 (map (lambda (x)
        (let* ((input (car x))
               (type (cdr x))
               (key (process-input input)))
          (define-test (format #f "checking type of ~s: ~a"
                               (printable-key key)
                               type)
            (pass-if-eq? (key-type key) type))))
      some-type-tests)
 (let ((k (process-input #\Z)))
   (define-test "normal character input: Z"
     (pass-if-eq? (key-type k) 'character))
   (define-test "key is not to ignore"
     (pass-if-false (key-ignore? k)))
   (define-test "key does not have meta set"
     (pass-if-false (key-meta? k))))
 (let ((k (process-input #\esc)))
   (define-test "escape character input"
     (pass-if-eq? (key-type k) 'character))
   (define-test "first escape *is* to ignore"
     (pass-if-true (key-ignore? k)))
   (define-test "key does not have meta set"
     (pass-if-false (key-meta? k))))
 (let ((k (process-input #\Z)))
   (define-test "normal character input again: Z"
     (pass-if-eq? (key-type k) 'character))
   (define-test "key is not to ignore"
     (pass-if-false (key-ignore? k)))
   (define-test "key *does* have meta set"
     (pass-if-true (key-meta? k))))
 (let ((k (process-input #\esc)))
   (define-test "escape character input"
     (pass-if-eq? (key-type k) 'character))
   (define-test "first escape *is* to ignore"
     (pass-if-true (key-ignore? k)))
   (define-test "key does not have meta set"
     (pass-if-false (key-meta? k))))
 (let ((k (process-input #\esc)))
   (define-test "escape character input"
     (pass-if-eq? (key-type k) 'character))
   (define-test "second escape is *not* to ignore"
     (pass-if-false (key-ignore? k)))
   (define-test "key *does* have meta set"
     (pass-if-true (key-meta? k)))))
