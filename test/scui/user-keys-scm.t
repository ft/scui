;; -*- scheme -*-

(use-modules (test tap)
             (scui user-key))

(force-import (scui user-key)
              parse-key-sequence
              user-key-boundary
              user-keys
              user-key-registered?)

(primitive-load "test/test-tap-cfg.scm")

(define some-keys `(("C-RETURN" "^[1234;2u" ,user-key-boundary)
                    ("C-BACKSPACE" "^[12;2u" ,(+ 1 user-key-boundary))
                    ("C-9" "^[57;5u" ,(+ 2 user-key-boundary))
                    ("C-TAB" "^[5;7u" ,(+ 3 user-key-boundary))
                    ("C-i" "^[6;7u" ,(+ 4 user-key-boundary))))

(with-fs-test-bundle
 (plan (+ 9
          (* 6 (length some-keys))))
 (define-test "key-is-user-key? #1"
   (pass-if-true (key-is-user-key? user-key-boundary)))
 (define-test "key-is-user-key? #2"
   (pass-if-true (key-is-user-key? (1+ user-key-boundary))))
 (define-test "key-is-user-key? #3"
   (pass-if-false (key-is-user-key? (1- user-key-boundary))))
 (map (lambda (x)
        (let ((name (car x))
              (seq (cadr x)))
          (define-test (format #f "(add-user-key ~a ~a)" name seq)
            (pass-if-eq? (add-user-key name seq)
                         'added))))
      some-keys)
 (define-test "add-user-key updates already added key"
   (pass-if-eq? (add-user-key "C-i" "foooo")
                'updated))
 (define-test "add-user-key updates already added key (back to list value)"
   (pass-if-eq? (add-user-key "C-i" (cadr (assoc "C-i" some-keys)))
                'updated))
 (define-test "add-user-key doesn't update unchanged definition"
   (pass-if-false (add-user-key "C-i" (cadr (assoc "C-i" some-keys)))))
 (map (lambda (x)
        (let ((name (car x))
              (id (caddr x)))
          (define-test (format #f "get-user-key-id ~a -> ~d" name id)
            (pass-if-= (get-user-key-id name) id))
          (define-test (format #f "id is user-key: ~d" id)
            (pass-if-true (key-is-user-key? id)))
          (define-test (format #f "get-user-key-name ~d -> ~a" id name)
            (pass-if-string=? (get-user-key-name id) name))))
      some-keys)
 ;; This adds another (length some-keys) tests.
 (hash-map->list
  (lambda (key val)
    (define-test (format #f "~a is not registered with ncurses" key)
      (pass-if-false (user-key-registered? val))))
  user-keys)
 (tap/comment "registering keys with ncurses...")
 (update-user-keys)
 ;; This adds another (length some-keys) tests.
 (hash-map->list
  (lambda (key val)
    (define-test (format #f "~a is now registered with ncurses" key)
      (pass-if-true (user-key-registered? val))))
  user-keys)
 (let ((seqs '("^[[13;5u" "\x1b[13;5u"
               (#\esc #\[ #\1 #\3 #\; #\5 #\u))))
   (let next ((seqs seqs) (i 1))
     (if (null? seqs)
         #f
         (begin
           (define-test (format #f "parse-key-sequence #~d" i)
             (pass-if-string=? "\x1b[13;5u" (parse-key-sequence (car seqs))))
           (next (cdr seqs) (1+ i)))))))
